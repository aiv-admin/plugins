<?php
/**
 *****************************************************************************************************************************************************
 * Google E-commerce Tracker for VirtueMart
 * Google E-commerce Tracker for VirtueMart inserts GTM dataLayer to track Virtuemart e-commerce checkout and order status change events. Requires free Google Tag Manager base plugin (plg_system_gtm)
 *
 * @version    Id: 1.2.0
 * @package    Joomla2.5.0 
 * @author     EasyJoomla.org <support@easyjoomla.org> 
 * @copyright  2014 EasyJoomla.org
 * @license    http://opensource.org/licenses/GPL-3.0 GPL-3.0
 * @link       http://www.easyjoomla.org
 * @generated  2014-08-08 at 09-17-42
 * @generator  Easy Joomla Extensions Generator by Viktor Jelínek <vikijel@gmail.com> at EasyJoomla.org <support@easyjoomla.org>
 *
 *****************************************************************************************************************************************************
 */
defined('_JEXEC') or die('Restricted access');
/**
 * Class JFormFieldVariable
 */
class JFormFieldVariable extends JFormField
{
	/**
	 * @var string
	 */
	var $type = 'variable';

	/**
	 * @return string
	 */
	function getInput()
	{
		$attr    = '';
		$options = array();

		$options[] = array('value' => '', 'text' => '');
		$options[] = array('value' => 'order_status', 'text' => JText::_('PLG_SYSTEM_GTM_VIRTUEMART_VARIABLE_ORDER_STATUS'));
		$options[] = array('value' => 'order_total', 'text' => JText::_('PLG_SYSTEM_GTM_VIRTUEMART_VARIABLE_ORDER_TOTAL'));
		$options[] = array('value' => 'order_number', 'text' => JText::_('PLG_SYSTEM_GTM_VIRTUEMART_VARIABLE_ORDER_NUMBER'));
		$options[] = array('value' => 'customer_number', 'text' => JText::_('PLG_SYSTEM_GTM_VIRTUEMART_VARIABLE_CUSTOMER_NUMBER'));
		$options[] = array('value' => 'virtuemart_user_id', 'text' => JText::_('PLG_SYSTEM_GTM_VIRTUEMART_VARIABLE_VIRTUEMART_USER_ID'));
		$options[] = array('value' => 'virtuemart_country_id', 'text' => JText::_('PLG_SYSTEM_GTM_VIRTUEMART_VARIABLE_VIRTUEMART_COUNTRY_ID'));
		$options[] = array('value' => 'virtuemart_shipmentmethod_id', 'text' => JText::_('PLG_SYSTEM_GTM_VIRTUEMART_VARIABLE_VIRTUEMART_SHIPMENTMETHOD_ID'));
		$options[] = array('value' => 'virtuemart_paymentmethod_id', 'text' => JText::_('PLG_SYSTEM_GTM_VIRTUEMART_VARIABLE_VIRTUEMART_PAYMENTMETHOD_ID'));

		return JHTML::_('select.genericlist', $options, $this->name, $attr, 'value', 'text', $this->value, $this->id);
	}

}