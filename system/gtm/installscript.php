<?php
/**
 *****************************************************************************************************************************************************
 * Google Tag Manager
 * Google Tag Manager (GTM) is plugin to insert GTM code into the site to be able to easily insert analytics and other google magic into your site.
 *
 * @package    Joomla 2.5.0
 * @author     EasyJoomla.org <support@easyjoomla.org>
 * @copyright  2014 EasyJoomla.org
 * @license    http://opensource.org/licenses/GPL-3.0 GPL-3.0
 * @link       http://www.easyjoomla.org
 * @generator  Easy Joomla Extensions Generator by Viktor Jelínek <vikijel@gmail.com> at EasyJoomla.org <support@easyjoomla.org>
 *
 *****************************************************************************************************************************************************
 */
defined('_JEXEC') or die('Restricted access');

/**
 * GTM system plugin installer
 *
 * @author Viktor Jelínek <vikijel@gmail.com>
 *
 * @since  2.0.0
 */
class plgSystemGtmInstallerScript
{
	public function postflight($route, JAdapterInstance $adapter)
	{
		if ($route != 'uninstall')
		{
			require_once JPATH_PLUGINS . '/system/gtm/helpers/queue.php';

			$queue = new GtmQueue();

			if (!$queue->createTable())
			{
				JFactory::getApplication()->enqueueMessage('Postflight: GtmQueue->createTable() failed', 'error');

				return false;
			}
		}

		return true;
	}

	public function preflight($route, JAdapterInstance $adapter)
	{
		if ($route == 'uninstall')
		{
			require_once JPATH_PLUGINS . '/system/gtm/helpers/queue.php';

			$queue = new GtmQueue();

			if (!$queue->dropTable())
			{
				JFactory::getApplication()->enqueueMessage('Postflight: GtmQueue->dropTable() failed', 'error');
			}

			try
			{
				$db = JFactory::getDbo();
				$q  = $db->getQuery(true)
						 ->update('#__extensions')
						 ->set('enabled = 0')
						 ->where("`type` = 'plugin'")
						 ->where("`element` LIKE 'gtm_%'");
				$db->setQuery($q);
				$db->execute();
			}
			catch (Exception $e)
			{
				JFactory::getApplication()->enqueueMessage('DB error: ' . $e->getMessage(), 'error');
			}
		}

		return true;
	}
}
