<?php
/**
 *****************************************************************************************************************************************************
 * Google E-commerce Tracker for VirtueMart
 * Google E-commerce Tracker for VirtueMart inserts GTM dataLayer to track Virtuemart e-commerce checkout and order status change events. Requires free Google Tag Manager base plugin (plg_system_gtm)
 *
 * @version    Id: 1.2.0
 * @package    Joomla2.5.0 
 * @author     EasyJoomla.org <support@easyjoomla.org> 
 * @copyright  2014 EasyJoomla.org
 * @license    http://opensource.org/licenses/GPL-3.0 GPL-3.0
 * @link       http://www.easyjoomla.org
 * @generated  2014-08-08 at 09-17-42
 * @generator  Easy Joomla Extensions Generator by Viktor Jelínek <vikijel@gmail.com> at EasyJoomla.org <support@easyjoomla.org>
 *
 *****************************************************************************************************************************************************
 */
defined('_JEXEC') or die('Restricted access');
/**
 * Class plgSystemGtm_virtuemart
 */
class plgSystemGtm_virtuemart extends JPlugin
{
	/**
	 * @var int
	 */
	public $gtm_params = null;

	/**
	 * @var bool
	 */
	public $gtm_initialized = false;

	/**
	 * @param object $subject
	 * @param array $config
	 */
	public function __construct($subject, $config)
	{
		parent::__construct($subject, $config);

		//$this->loadLanguage();

		$this->session    = JFactory::getSession();
		$this->gtm_params = $this->getGtmParams();

		if ($this->gtm_params !== null)
		{
			if ((int)$this->gtm_params->get('write_log', 0))
			{
				jimport('joomla.log.log');
				JLog::addLogger(array('text_file' => 'plg_system_gtm_virtuemart.log.php'), JLog::ALL, array('plg_system_gtm_virtuemart'));
			}

			$this->gtm_initialized = true;

			if (trim($this->gtm_params->get('container_id', '')) == '')
			{
				$this->loadLanguage();
				JFactory::getApplication()->enqueueMessage(JText::_('PLG_SYSTEM_GTM_VIRTUEMART_MSG_INSTALL_SET_UP_AND_ENABLE_GTM_PLUGIN'), 'error');
				$this->gtm_initialized = false;
			}
		}
		else
		{
			$this->loadLanguage();
			JFactory::getApplication()->enqueueMessage(JText::_('PLG_SYSTEM_GTM_VIRTUEMART_MSG_INSTALL_SET_UP_AND_ENABLE_GTM_PLUGIN'), 'error');
			$this->gtm_initialized = false;
		}
	}

	/**
	 * @return JRegistry|null
	 */
	public function getGtmParams()
	{
		if ($this->gtm_params == null)
		{
			$this->gtm_params = new JRegistry();

			$db = JFactory::getDbo();
			$q  = $db->getQuery(true);

			$q->select('`params`');
			$q->from('`#__extensions`');
			$q->where("`element`  = 'gtm'");
			$q->where("`type`  = 'plugin'");
			$q->where("`folder`  = 'system'");
			$q->where("`enabled`  = 1");

			$db->setQuery($q);

			$gtm_params = $db->loadResult();

			if (empty($gtm_params))
			{
				$this->gtm_params = null;
			}
			else
			{
				$this->gtm_params->loadString($gtm_params, 'JSON');
			}
		}

		return $this->gtm_params;
	}

	/**
	 * @param object $cart
	 * @param array $order
	 *
	 * @return bool|null
	 */
	public function plgVmConfirmedOrder($cart, $order)
	{
		$this->writeLog(__METHOD__ . ": \n" . print_r(func_get_args(), true));

		if (!$this->gtm_initialized or (!(int)$this->gtm_params->get('enable_backend', 1) and JFactory::getApplication()->isAdmin()) or !(int)$this->params->get('track_checkout', 1))
		{
			return null;
		}

		$data = $this->getDataForOrder($order['details']['BT']->virtuemart_order_id, $this->params->get('event_checkout', 'ecomEvent'), 'checkout', 'id:number', $this->params->get('track_custom_vars_extra', 1));

		if (empty($data))
		{
			return null;
		}

		$this->pushToDataLayer($data);

		return true;
	}
	
	/**
	 * Alias for one page checkout component, which does not trigger plgVmConfirmedOrder for system plugins
	 * 	 
	 * @param object $cart
	 * @param array $order
	 *
	 * @return bool|null
	 */	 	 	 	
	public function plgOpcOrderCreated($cart, $order)
	{
		$this->writeLog(__METHOD__ . ": \n" . print_r(func_get_args(), true));
	
		return $this->plgVmConfirmedOrder($cart, $order);
	}

	/**
	 * @param array $data
	 * @param string $old_status
	 *
	 * @return bool|null
	 */
	public function plgVmCouponUpdateOrderStatus($data, $old_status)
	{
		$this->writeLog(__METHOD__ . ": \n" . print_r(func_get_args(), true));

		if (!$this->gtm_initialized or (!(int)$this->gtm_params->get('enable_backend', 1) and JFactory::getApplication()->isAdmin()) or !(int)$this->params->get('track_orderstatus', 1))
		{
			return null;
		}

		$data = $this->getDataForOrder($data->virtuemart_order_id, $this->params->get('event_orderstatus', 'orderStatusUpdate'), 'orderstatus', 'id:number:status', $this->params->get('track_custom_vars_extra', 1));

		if (empty($data))
		{
			return null;
		}

		$data['transactionStatusOld'] = $old_status;

		$this->pushToDataLayer($data);

		return true;
	}

	/**
	 * @param int $virtuemart_order_id
	 * @param string $event
	 * @param string $type
	 * @param string $transid_default
	 * @param bool $push_custom_vars
	 *
	 * @return array|null
	 */
	private function getDataForOrder($virtuemart_order_id, $event = 'ecomEvent', $type = 'checkout', $transid_default = 'id:number', $push_custom_vars = true)
	{
		$this->writeLog(__METHOD__ . ": \n" . print_r(func_get_args(), true));

		if (!class_exists('VirtueMartModelCategory'))
		{
			require(JPATH_ROOT . '/administrator/components/com_virtuemart/models/category.php');
		}

		if (!class_exists('VirtueMartModelOrders'))
		{
			require(JPATH_ROOT . '/administrator/components/com_virtuemart/models/orders.php');
		}

		$modelCategory = new VirtueMartModelCategory();
		$modelOrder    = new VirtueMartModelOrders();
		$order         = $modelOrder->getOrder($virtuemart_order_id);
		$transid_rep   = array(
			'id'     => 'virtuemart_order_id',
			'number' => 'order_number',
			'status' => 'order_status',
		);

		if ($type == 'orderstatus')
		{

			$statuses_only = (array)$this->params->get('tracked_statuses', null, 'array');

			if (!empty($statuses_only) and !in_array($order['details']['BT']->order_status, $statuses_only))
			{
				return null;
			}
		}

		$transactionId = $this->params->get('transid_' . $type, $transid_default);

		foreach ($transid_rep as $find => $var)
		{
			$transactionId = str_replace($find, (isset($order['details']['BT']->$var) ? $order['details']['BT']->$var : ''), $transactionId);
		}

		$data = array(
			'event'                  => $event,
			'transactionId'          => $transactionId,
			'transactionAffiliation' => 'Virtuemart',
			'transactionTotal'       => $order['details']['BT']->order_total,
			'transactionCurrency'    => $this->getCurrencyByID($order['details']['BT']->order_currency, 'currency_code_3'),
			'transactionTax'         => $order['details']['BT']->order_tax,
			'transactionShipping'    => ($order['details']['BT']->order_shipment + $order['details']['BT']->order_payment),
			'transactionCity'        => $order['details']['BT']->city,
			'transactionState'       => $order['details']['BT']->virtuemart_state_id,
			'transactionCountry'     => $order['details']['BT']->virtuemart_country_id,
			'transactionStatus'      => $order['details']['BT']->order_status,
			'transactionProducts'    => array(),
		);

		foreach ($order['items'] as $item)
		{

			$category                      = $modelCategory->getCategory($item->virtuemart_category_id);
			$data['transactionProducts'][] = array(
				'sku'      => $item->order_item_sku,
				'name'     => $item->order_item_name,
				'category' => $category->category_name,
				'price'    => $item->product_final_price,
				'quantity' => $item->product_quantity,
			);
		}

		$custom_variables = $this->getCustomVariables($order);

		if (!empty($custom_variables))
		{

			$data = array_merge($data, $custom_variables);

			if ($push_custom_vars)
			{
				$custom_variables['event'] = 'customVariables';
				$this->pushToDataLayer($custom_variables);
			}
		}

		return $data;
	}

	/**
	 * @param array $order
	 *
	 * @return array
	 */
	private function getCustomVariables($order)
	{
		$data = array();

		for ($i = 1; $i <= 5; $i++)
		{
			$variable = trim($this->params->get('variable' . $i, ''));

			if ($variable != '')
			{
				$value = '';

				switch ($variable)
				{

					default:
						$value = (isset($order['details']['BT']->$variable) ? $order['details']['BT']->$variable : '');
						break;
				}

				$data["$variable"] = $value;
			}
		}

		return $data;
	}

	/**
	 * @param array $data
	 *
	 * @return bool|null
	 */
	private function pushToDataLayer($data = array())
	{
		$this->writeLog(__METHOD__ . ": \n" . print_r(func_get_args(), true));

		if (empty($data))
		{
			return null;
		}

		$gtm_js   = (array)$this->session->get('plgSystemGtm_js');
		$gtm_js[] = 'dataLayer.push(' . json_encode($data) . ');';

		$this->session->set('plgSystemGtm_js', $gtm_js);

		return true;
	}

	/* Copied from VM file administrator/components/com_virtuemart/helpers/shopfunctions.php (because it is not in all versions of VM) */
	/**
	 * @param int $id
	 * @param string $fld
	 *
	 * @return string
	 */
	private function getCurrencyByID($id, $fld = 'currency_name')
	{
		if (empty($id))
		{
			return '';
		}

		$id = (int)$id;
		$db = JFactory::getDBO();

		$q = 'SELECT `' . $db->escape($fld) . '` AS fld FROM `#__virtuemart_currencies` WHERE `virtuemart_currency_id` = ' . (int)$id;
		$db->setQuery($q);

		return $db->loadResult();
	}

	/**
	 * @param string $text
	 * @param int $type
	 */
	private function writeLog($text, $type = JLog::INFO)
	{
		if ((int)$this->gtm_params->get('write_log', 0))
		{
			JLog::add($text, $type, 'plg_system_gtm_virtuemart');
		}
	}
}

?>
