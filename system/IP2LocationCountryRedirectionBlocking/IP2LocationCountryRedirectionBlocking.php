<?php
/**
 * @version		IP2LocationCountryRedirectionBlocking.php  1.2.0
 *
 * @copyright	Copyright (C) IP2Location 2018
 * @license		GNU/GPL, see LICENSE.php
 */
defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');

class plgSystemIP2LocationCountryRedirectionBlocking extends JPlugin
{
	public $params;

	public function onAfterDispatch()
	{
		$app = JFactory::getApplication();
		$plugin = JPluginHelper::getPlugin('system', 'IP2LocationCountryRedirectionBlocking');
		$this->params = new JRegistry();
		$this->params->loadString($plugin->params);

		$mode = $this->params->get('mode');
		$redirectedUrl = $this->params->get('redirected_url');
		$apiKey = $this->params->get('api_key');

		if ($this->isInPluginEditPage()) {
			$db = JFactory::getDBO();
			$db->setQuery('SELECT extension_id FROM #__extensions WHERE folder = "system" AND element = "IP2LocationCountryRedirectionBlocking"');
			$plg = $db->loadObject();

			if ($plg->extension_id == $app->input->get('extension_id') && empty($apiKey)) {
				// Looking for .BIN file in plugin  directory
				$files = scandir(JPATH_PLUGINS . '/system/IP2LocationCountryRedirectionBlocking');

				foreach ($files as $file) {
					if (strtoupper(substr($file, -4)) == '.BIN') {
						if (!class_exists('IP2Location')) {
							require JPATH_PLUGINS . '/system/IP2LocationCountryRedirectionBlocking/ip2location.class.php';
						}

						$ipl = new \IP2Location\Database((JPATH_PLUGINS . '/system/IP2LocationCountryRedirectionBlocking/' . $file), \IP2Location\Database::FILE_IO);
						$dbVersion = $ipl->getDatabaseVersion();
						$curdbVersion = str_replace('.', '-', $dbVersion);

						if (strtotime($curdbVersion) < strtotime('-2 months')) {
							JFactory::getApplication()->enqueueMessage('Your IP2Location database is out-dated. Please download the latest version from http://lite.ip2location.com (Free) or http://www.ip2location.com (Commercial).', 'notice');
						}
						break;
					}
				}
			}

			if ((strcmp($mode, '1') == 0)) {
				if (empty($redirectedUrl)) {
					JFactory::getApplication()->enqueueMessage('Please provide a valid URL for redirection.', 'notice');
				} elseif (!filter_var($redirectedUrl, FILTER_VALIDATE_URL)) {
					JFactory::getApplication()->enqueueMessage('Invalid URL provided.', 'notice');
				}
			}
		}

		if (!$app->isSite()) {
			return;
		}

		$countries = $this->params->get('blocked_countries');

		if (is_array($countries)) {
			if (in_array($this->getCountryCodeByIP(), $countries)) {
				if ((strcmp($mode, '1') == 0) && (!empty($redirectedUrl)) && (filter_var($redirectedUrl, FILTER_VALIDATE_URL))) {
					header('HTTP/1.1 301 Moved Permanently');
					header('Location: ' . $redirectedUrl);
					jexit();
				} else {
					header('HTTP/1.1 403 Forbidden');
					echo $this->params->get('message');
					jexit();
				}
			}
		}
	}

	private function getIP()
	{
		// For development purpose
		if (isset($_SERVER['DEV_MODE'])) {
			return '8.8.8.8';
		}

		if (isset($_SERVER['HTTP_CF_CONNECTING_IP']) && filter_var($_SERVER['HTTP_CF_CONNECTING_IP'], FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE)) {
			return $_SERVER['HTTP_CF_CONNECTING_IP'];
		}

		if (isset($_SERVER['X-Real-IP']) && filter_var($_SERVER['X-Real-IP'], FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE)) {
			return $_SERVER['X-Real-IP'];
		}

		if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = trim(current(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR'])));

			if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE)) {
				return $ip;
			}
		}

		return $_SERVER['REMOTE_ADDR'];
	}

	private function getCountryCodeByIP()
	{
		if (!class_exists('IP2Location')) {
			require JPATH_PLUGINS . '/system/IP2LocationCountryRedirectionBlocking/ip2location.class.php';
		}

		if (!empty($this->params->get('api_key'))) {
			$response = $this->getHttpResponse('https://api.ip2location.com/?' . http_build_query([
				'key'     => $this->params->get('api_key'),
				'ip'      => $this->getIP(),
				'format'  => 'json',
				'package' => 'WS1',
			]));

			if (($json = json_decode($response)) !== null) {
				return $json->country_code;
			}
		} else {
			// Looking for .BIN file in plugin  directory
			$files = scandir(JPATH_PLUGINS . '/system/IP2LocationCountryRedirectionBlocking');

			foreach ($files as $file) {
				if (strtoupper(substr($file, -4)) == '.BIN') {
					$geo = new \IP2Location\Database((JPATH_PLUGINS . '/system/IP2LocationCountryRedirectionBlocking/' . $file), \IP2Location\Database::FILE_IO);

					// Get geolocation by IP address.
					$response = $geo->lookup($this->getIP(), \IP2Location\Database::ALL);

					return $response['countryCode'];
				}
			}
		}
	}

	private function getHttpResponse($url)
	{
		$http = curl_init();

		curl_setopt($http, CURLOPT_FAILONERROR, true);
		curl_setopt($http, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($http, CURLOPT_AUTOREFERER, true);
		curl_setopt($http, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($http, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($http, CURLOPT_ENCODING, 'gzip, deflate');
		curl_setopt($http, CURLOPT_HTTP_VERSION, '1.1');
		curl_setopt($http, CURLOPT_USERAGENT, 'IP2Location-Joomla');
		curl_setopt($http, CURLOPT_TIMEOUT, 60);

		curl_setopt($http, CURLOPT_URL, $url);
		curl_setopt($http, CURLOPT_HTTPGET, true);

		$response = curl_exec($http);

		if (!curl_errno($http)) {
			return $response;
		}

		return null;
	}

	private function isInPluginEditPage()
	{
		$app = JFactory::getApplication();

		if ($app->input->get('option', '') == 'com_plugins' && ($app->input->get('view', '') == 'plugin' || $app->input->get('view', '') == '')) {
			return true;
		}

		return false;
	}
}
