<?php

/**
 * @package         Responsive Scroll Triggered Box
 * @author          Tassos Marinos <info@tassos.gr>
 * @link            http://www.tassos.gr
 * @copyright       Copyright © 2015 TM Extensions All Rights Reserved
 * @license         http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.plugin.plugin' );

class PlgSystemRstBox extends JPlugin
{
    public static $boxesHTML;
    private $_app;
    private $_params;
    private $_view;

    public function __construct(&$subject, $config = array()) {
        
        // get application & component params  
        $this->_app = JFactory::getApplication();
        $this->_params = JComponentHelper::getParams('com_rstbox');

        $input = JFactory::getApplication()->input;
        $this->_view = $input->get('format','html','cmd');

        // execute parent constructor
        parent::__construct($subject, $config);
    }

    public function onAfterRoute() {

        /* Do nothing on back end or if format is not html */
        if (($this->_app->isAdmin()) || ($this->_view != 'html')) return;

        /* Load RSTBox Helper class */
        JLoader::register('RstboxHelper', JPATH_ADMINISTRATOR.'/components/com_rstbox/helpers/rstbox.php');

        // Set Session Start Time used by Time-on-Site publishing assignment
        RstboxHelper::SessionStartTime();

        /* Get all boxes based on active menu id */
        $boxes = RstboxHelper::getBoxes();

        /* Check if is there at least one box and then load css and js files */
        if ($boxes) {
            if (!$this->_params->get("forceloadmedia", false)) {
                RstboxHelper::loadassets_front();
            }

            /* Prepare HTML */
            $html = RstboxHelper::renderLayout("rstbox", $boxes);
            self::$boxesHTML = $html;
        }
    }

    public function onAfterRender() {
        
        /* Do nothing on back end or if format is not html */
        if (($this->_app->isAdmin()) || ($this->_view != 'html')) return;

        if (!self::$boxesHTML) {
            return;
        }

        /* Prepare some vars */
        $closingTag = "</".trim($this->_params->get("replaceclosetag", "body")).">";
        $buffer = JResponse::getBody();

        // Last check if it's really an HTML document
        if (strpos($buffer, "<?xml") !== false) {
            return;
        }

        $closingTagExist = strpos($buffer, $closingTag);
        $html = self::$boxesHTML;

        // Prepare html with Content Plugins
        if ($this->_params->get("preparecontent", 0)) {
            $html = JHtml::_('content.prepare', $html);
        }


        // Find the closing tag to append the html
        if ($closingTagExist) {
            $buffer = str_replace($closingTag, $html.$closingTag, $buffer);
        } else {
            $buffer .= $html;
        }
        
        // Set body's finall layout
        JResponse::setBody($buffer);
    }
}
