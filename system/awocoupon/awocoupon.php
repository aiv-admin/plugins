<?php
/**
 * @component AwoCoupon Pro
 * @copyright Copyright (C) Seyi Awofadeju - All rights reserved.
 * @Website : http://awodev.com
 **/

defined( '_JEXEC' ) or die( 'Restricted access' );


jimport('joomla.event.plugin');
class plgSystemAwoCoupon extends JPlugin {

	public function onAfterRoute() {
	
		$this->checkCron();
		
		$this->checkCouponFromLink();
		
	}
	
	
	protected function checkCron() {
		if( ! class_exists('awoParams') ) {
			if( ! file_exists(JPATH_ADMINISTRATOR.'/components/com_awocoupon/helpers/awoparams.php') ) return;
			require JPATH_ADMINISTRATOR.'/components/com_awocoupon/helpers/awoparams.php';
		}
		/*{ // verify config table exists
			$p__ = JFactory::getConfig()->{version_compare( JVERSION, '1.6.0', 'ge' ) ? 'get' : 'getValue'} ( 'dbprefix' );
			$db = JFactory::getDbo();
			$db->setQuery('SHOW TABLES LIKE "'.$p__.'awocoupon_config"');
			$test = $db->loadResult();
			if(empty($test)) return;
		}*/
		$awoparams = new awoParams();
		if((int)$awoparams->get('cron_enable', 0) != 1) return;

		$document = JFactory::getDocument();
		$cron_url = JURI::root(true).'/index.php?option=com_awocoupon&task=cronjs&time='.time();
		$document->addScriptDeclaration ( '
			window.addEventListener("load", function (){
				var myelement = document.createDocumentFragment();
				var temp = document.createElement("div");
				temp.innerHTML = \'<img src="'.$cron_url.'" alt="" width="0" height="0" style="border:none;margin:0; padding:0"/>\';
				while (temp.firstChild) {
					myelement.appendChild(temp.firstChild);
				}
				document.body.appendChild(myelement);
			});
		');
	}


    protected function checkCouponFromLink() {
	
		if (JFactory::getApplication()->isAdmin()) return; 
	  
		$session = JFactory::getSession();

		$code = JRequest::getVar('addcoupontocart');
		if(!empty($code)) {
			$session->set('link_coupon_code', $code, 'awocoupon'); 	
			$session->set('link_coupon_code_processed', false, 'awocoupon');
		}
		
		$coupon_code = $session->get('link_coupon_code', '', 'awocoupon');
		if(empty($coupon_code)) return;
		
		
		
		if(!defined('AWOCOUPON_ESTORE')) {
			if( ! file_exists(JPATH_ADMINISTRATOR.'/components/com_awocoupon/awocoupon.config.php') ) return;
			require JPATH_ADMINISTRATOR.'/components/com_awocoupon/awocoupon.config.php';
		}
		if(!defined('AWOCOUPON_ESTORE')) return;
		
		call_user_func(array(AWOCOUPON_ESTOREHELPER,'addCouponFromLinkToCart'));
		
	}

}
