<?php
/**
 *****************************************************************************************************************************************************
 * Google Tag Manager
 * Google Tag Manager (GTM) is plugin to insert GTM code into the site to be able to easily insert analytics and other google magic into your site.
 *
 * @package    Joomla 2.5.0
 * @author     EasyJoomla.org <support@easyjoomla.org>
 * @copyright  2014 EasyJoomla.org
 * @license    http://opensource.org/licenses/GPL-3.0 GPL-3.0
 * @link       http://www.easyjoomla.org
 * @generated  2014-07-30 at 13-42-27
 * @generator  Easy Joomla Extensions Generator by Viktor Jelínek <vikijel@gmail.com> at EasyJoomla.org <support@easyjoomla.org>
 *
 *****************************************************************************************************************************************************
 */
defined('_JEXEC') or die('Restricted access');

if (!class_exists('GtmQueue'))
{
	require JPATH_PLUGINS . '/system/gtm/helpers/queue.php';
}

/**
 * GTM Plugin helper
 */
abstract class GtmPlugin extends JPlugin
{
	/** @var \Joomla\Registry\Registry|JRegistry */
	protected $gtm_params = null;

	/** @var bool */
	protected $gtm_initialized = false;

	/** @var GtmQueue */
	protected $gtm_queue = null;

	/**
	 * Constructor
	 *
	 * @param   object $subject Subject
	 * @param   array  $config  Config
	 */
	public function __construct($subject, $config)
	{
		parent::__construct($subject, $config);

		$this->getGtmParams();

		if ($this->gtm_params !== null)
		{
			$this->gtm_queue = new GtmQueue();

			if ((int) $this->gtm_params->get('write_log', 0))
			{
				jimport('joomla.log.log');
				JLog::addLogger(
					array('text_file' => 'plg_' . $this->_type . '_' . $this->_name . '.log.php'),
					JLog::ALL,
					array('plg_' . $this->_type . '_' . $this->_name)
				);
			}

			if (trim($this->gtm_params->get('container_id', '')) != '')
			{
				$this->gtm_initialized = true;
			}
		}
	}

	/**
	 * @param array $data
	 *
	 * @return bool|null
	 */
	protected function pushToDataLayer($data = array())
	{
		$this->writeLog(__METHOD__ . ": \n" . print_r(func_get_args(), true));

		if (empty($data) or !$this->gtm_initialized)
		{
			return null;
		}

		return $this->gtm_queue->addItem($data, 'plg_' . $this->_type . '_' . $this->_name);
	}

	/**
	 * @return \Joomla\Registry\Registry|JRegistry|null
	 */
	protected function getGtmParams()
	{
		if ($this->gtm_params == null)
		{
			$this->gtm_params = new JRegistry();

			$db = JFactory::getDbo();
			$q  = $db->getQuery(true);

			$q->select('`params`');
			$q->from('`#__extensions`');
			$q->where("`element`  = 'gtm'");
			$q->where("`type`  = 'plugin'");
			$q->where("`folder`  = 'system'");
			$q->where("`enabled`  = 1");

			$db->setQuery($q);

			$gtm_params = $db->loadResult();

			if (!empty($gtm_params))
			{
				$this->gtm_params->loadString($gtm_params, 'JSON');
			}
		}

		return $this->gtm_params;
	}

	/**
	 * @param string $text
	 * @param int    $type
	 */
	protected function writeLog($text, $type = JLog::INFO)
	{
		if ((int) $this->gtm_params->get('write_log', 0))
		{
			JLog::add($text, $type, 'plg_' . $this->_type . '_' . $this->_name);
		}
	}
}