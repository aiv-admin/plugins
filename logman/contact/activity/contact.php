<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * Contact Activity Entity
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Plugin\LOGman
 */
class PlgLogmanContactActivityContact extends ComLogmanModelEntityActivity
{
    protected function _initialize(KObjectConfig $config)
    {
        $objects = array();

        if ($config->data->action == 'contact')
        {
            $format = '{actor} {action} {object} {name}';
            $objects[] = 'name';
        }
        else $format = '{actor} {action} {object.type} name {object}';

        $config->append(array(
            'objects'       => $objects,
            'format'        => $format,
            'object_table'  => 'contact_details',
            'object_column' => 'id'
        ));

        parent::_initialize($config);
    }

    public function getPropertyImage()
    {
        if ($this->verb == 'contact') {
            $image = 'icon-envelope';
        } else {
            $image = parent::getPropertyImage();
        }

        return $image;
    }

    public function getActivityName()
    {
        $metadata = $this->getMetadata();

        $url = $this->getObject('lib:http.url', array('url' => 'mailto:' . $metadata->sender->email));

        $config   = array(
            'objectName' => $metadata->sender->name,
            'url'        => $url
        );

        return $this->_getObject($config);
    }
}
