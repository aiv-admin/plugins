<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * Category/Categories Activity Entity
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Plugin\LOGman
 */
class PlgLogmanCategoriesActivityCategory extends ComLogmanModelEntityActivity
{
    protected function _initialize(KObjectConfig $config)
    {
        $config->append(array(
            'object_table'  => 'categories',
            'object_column' => 'id'
        ));

        if ($config->data->metadata) {
            $config->append(array('format' => '{actor} {action} {object.subtype} {object.type} title {object}'));
        }

        parent::_initialize($config);
    }

    protected function _objectConfig(KObjectConfig $config)
    {
        parent::_objectConfig($config);

        $metadata = $this->getMetadata();

        if ($metadata)
        {
            $config->url = $config->url .'&extension=' . $metadata->extension;

            // Set subtype.
            $config->subtype = array('objectName' => $this->_getSubtype(), 'object' => true);
        }
    }

    protected function _getSubtype()
    {
        $extension = $this->getMetadata()->extension;

        if (strpos($extension, '.') === false)
        {
            // Guess context based on provided extension.
            // J!2.5 only provides the extension name while v3 passes (ON SOME CASES) a context.
            switch ($extension)
            {
                case 'com_users':
                    $context = 'com_users.notes';
                    break;
                case 'com_content':
                    $context = 'com_content.articles';
                    break;
                case 'com_banners':
                    $context = 'com_banners.banners';
                    break;
                case 'com_contact':
                    $context = 'com_contact.contacts';
                    break;
                case 'com_newsfeeds':
                    $context = 'com_newsfeeds.newsfeeds';
                    break;
                case 'com_weblinks':
                    $context = 'com_weblinks.weblinks';
                    break;
                default:
                    $context = null;
                    break;
            }
        }
        else $context = $extension;

        // Translate context into readable type.
        switch ($context)
        {
            case 'com_users.notes':
                $subtype = 'user notes';
                break;
            case 'com_content.articles':
                $subtype = 'articles';
                break;
            case 'com_banners.banners':
                $subtype = 'banners';
                break;
            case 'com_contact.contacts':
                $subtype = 'contacts';
                break;
            case 'com_newsfeeds.newsfeeds':
                $subtype = 'newsfeeds';
                break;
            case 'com_weblinks.weblinks':
                $subtype = 'weblinks';
                break;
            default:
                $subtype = '';
                break;
        }

        if (!$subtype)
        {
            if (strpos($extension, '.') !== false) {
                $subtype = substr($extension, 0, strpos($extension, '.'));
            } else {
                $subtype = $extension;
            }

            // Load the component translations.
            $this->_loadTranslations($subtype);
        }

        return $subtype;
    }
}