<?php
/**
 *****************************************************************************************************************************************************
 * Google E-commerce Tracker for VirtueMart
 * Google E-commerce Tracker for VirtueMart inserts GTM dataLayer to track Virtuemart e-commerce checkout and order status change events. Requires free Google Tag Manager base plugin (plg_system_gtm)
 *
 * @version    Id: 1.2.0
 * @package    Joomla2.5.0 
 * @author     EasyJoomla.org <support@easyjoomla.org> 
 * @copyright  2014 EasyJoomla.org
 * @license    http://opensource.org/licenses/GPL-3.0 GPL-3.0
 * @link       http://www.easyjoomla.org
 * @generated  2014-08-08 at 09-17-42
 * @generator  Easy Joomla Extensions Generator by Viktor Jelínek <vikijel@gmail.com> at EasyJoomla.org <support@easyjoomla.org>
 *
 *****************************************************************************************************************************************************
 */
defined('_JEXEC') or die('Restricted access');
defined('DS') or DEFINE('DS', DIRECTORY_SEPARATOR);

/**
 * Class JFormFieldOrderstatus
 */
class JFormFieldOrderstatus extends JFormField
{
	/**
	 * @var string
	 */
	var $type = 'orderstatus';

	/**
	 * @return string
	 */
	function getInput()
	{
		if (!file_exists(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart' . DS . 'helpers' . DS . 'config.php'))
		{
			return '<span class="fltlft red">Virtuemart not installed</span>';
		}
		if (!class_exists('VmConfig'))
		{
			require(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart' . DS . 'helpers' . DS . 'config.php');
		}
		if (!class_exists('VmModel'))
		{
			require(JPATH_VM_ADMINISTRATOR . DS . 'helpers' . DS . 'vmmodel.php');
		}

		VmConfig::loadConfig();

		if (VmConfig::get('enableEnglish', 1))
		{
			$jlang = JFactory::getLanguage();
			$jlang->load('com_virtuemart', JPATH_ADMINISTRATOR, 'en-GB', true);
			$jlang->load('com_virtuemart', JPATH_ADMINISTRATOR, $jlang->getDefault(), true);
			$jlang->load('com_virtuemart', JPATH_ADMINISTRATOR, null, true);
		}

		$key         = ($this->element['key_field'] ? $this->element['key_field'] : 'value');
		$val         = ($this->element['value_field'] ? $this->element['value_field'] : $this->name);
		$model       = VmModel::getModel('Orderstatus');
		$orderStatus = $model->getOrderStatusList();

		foreach ($orderStatus as $orderState)
		{
			$orderState->order_status_name = JText::_($orderState->order_status_name);
		}

		$attr = 'class="inputbox"';

		if (isset($this->element['multiple']) and $this->element['multiple'] != 'false')
		{
			$attr .= ' multiple="true"';
		}

		if (isset($this->element['size']) and (int)$this->element['size'])
		{
			$attr .= ' size="' . (int)$this->element['size'] . '"';
		}

		return JHTML::_('select.genericlist', $orderStatus, $this->name, $attr, 'order_status_code', 'order_status_name', $this->value, $this->id);
	}
}