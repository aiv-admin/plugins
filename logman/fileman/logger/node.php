<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * Nodde FILEman Logger
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Plugin\LOGman
 */
abstract class PlgLogmanFilemanLoggerNode extends ComLogmanActivityLogger
{
    protected function _initialize(KObjectConfig $config)
    {
        $config->append(array(
            'title_column' => 'name'
        ));

        parent::_initialize($config);
    }

    public function log($action, KModelEntityInterface $object, KObjectIdentifierInterface $subject)
    {
        if ($subject->package == 'fileman') {
            parent::log($action, $object, $subject);
        }
    }

    public function getActivitySubject(KCommandInterface $context)
    {
        $identifier = $context->subject->getIdentifier()->toArray();

        $container = explode('-', $context->result->container);
        $container = $container[0];

        if ($container) {
            $identifier['package'] = $container;
        }

        return $this->getIdentifier($identifier);
    }

    public function getActivityData(KModelEntityInterface $object, KObjectIdentifierInterface $subject)
    {
        $data = parent::getActivityData($object, $subject);

        $container = $object->getContainer();

        // Use container:path as row identifier.
        $data['row'] = $object->container . ':' . $object->path;

        // Get the application name from Joomla!.
        $data['application'] = JFactory::getApplication()->getName() == 'site' ? 'site' : 'admin';

        $data['metadata'] = array(
            'name'      => $object->name,
            'folder'    => $object->folder,
            'path'      => $object->path,
            'container' => array(
                'id'    => $container->id,
                'slug'  => $container->slug,
                'title' => $container->title
            )
        );

        return $data;
    }
}