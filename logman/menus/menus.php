<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * Menus LOGman Plugin.
 *
 * Provides event handlers for dealing with com_menus events.
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Plugin\LOGman
 */
class PlgLogmanMenus extends ComLogmanPluginJoomla
{
    protected function _initialize(KObjectConfig $config)
    {
        $config->append(array(
            'resources' => array('menu', 'item')
        ));

        parent::_initialize($config);
    }

    protected function _getItems($ids, KObjectConfig $config)
    {
        if ($config->type == 'item')
        {
            $config->prefix = 'MenusTable';
            $config->type   = 'Menu';
        }

        return parent::_getItems($ids, $config);
    }
}