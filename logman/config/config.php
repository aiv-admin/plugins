<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * Config LOGman Plugin.
 *
 * Provides event handlers for dealing with com_config events.
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Plugin\LOGman
 */
class PlgLogmanConfig extends ComLogmanPluginJoomla
{
    protected function _initialize(KObjectConfig $config)
    {
        $config->append(array('resources' => array('component')));
        parent::_initialize($config);
    }

    /**
     * Joomla 2.5 After Configuration Save Event Handler
     *
     * @param $context
     * @param $extension
     * @param $isNew
     * @throws Exception
     */
    public function onConfigurationAfterSave($context, $extension, $isNew)
    {
        $this->log(array(
            'object' => array(
                'package' => 'config',
                'type'    => 'settings',
                'id'      => $extension->extension_id,
                'name'    => $extension->element),
            'verb'   => 'edit'
        ));
    }

    protected function _getComponentObjectData($data, $event)
    {
        return array('name' => $data->element, 'id' => $data->extension_id);
    }


}