<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * FILEman LOGman Plugin.
 *
 * Wires FILEman loggers to com_files controllers.
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Plugin\LOGman
 */
class PlgLogmanFileman extends ComLogmanPluginKoowa
{
    protected function _initialize(KObjectConfig $config)
    {
        $config->append(array(
            'controllers' => array(
                'com:files.controller.file'   => 'plg:logman.fileman.logger.file',
                'com:files.controller.folder' => 'plg:logman.fileman.logger.folder'
            )
        ));

        parent::_initialize($config);
    }
}