<?php
/**
 *****************************************************************************************************************************************************
 * Google Tag Manager
 * Google Tag Manager (GTM) is plugin to insert GTM code into the site to be able to easily insert analytics and other google magic into your site.
 *
 * @package    Joomla 2.5.0
 * @author     EasyJoomla.org <support@easyjoomla.org>
 * @copyright  2014 EasyJoomla.org
 * @license    http://opensource.org/licenses/GPL-3.0 GPL-3.0
 * @link       http://www.easyjoomla.org
 * @generated  2014-07-30 at 13-42-27
 * @generator  Easy Joomla Extensions Generator by Viktor Jelínek <vikijel@gmail.com> at EasyJoomla.org <support@easyjoomla.org>
 *
 *****************************************************************************************************************************************************
 */
defined('_JEXEC') or die('Restricted access');

/**
 * GTM Queue helper
 */
class GtmQueue
{
	/** @var JDatabaseDriver */
	protected $db;

	/**
	 * Init queue
	 */
	public function __construct()
	{
		$this->db = JFactory::getDbo();
	}

	/**
	 * @param array|object $data   Structure to be pushed into datalayer
	 * @param string       $plugin Name of plugin
	 *
	 * @return bool
	 */
	public function addItem($data = array(), $plugin = '')
	{
		try
		{
			$row = (object) array(
				'plugin'  => $plugin,
				'json'    => json_encode($data),
				'created' => JFactory::getDate()->toSql()
			);

			$this->db->insertObject('#__gtm_queue', $row);

			return true;
		}
		catch (Exception $e)
		{
			JFactory::getApplication()->enqueueMessage('DB error: ' . $e->getMessage(), 'error');

			return false;
		}
	}

	/**
	 * @param array  $ids
	 * @param string $status
	 *
	 * @return bool
	 */
	public function updateItemsStatus($ids, $status = 'tracked')
	{
		$ids = (array) $ids;

		JArrayHelper::toInteger($ids);

		$ids = array_filter($ids, 'intval');

		if (empty($ids))
		{
			return true;
		}

		try
		{
			$q = $this->db->getQuery(true)
						  ->update('#__gtm_queue')
						  ->set('`status` = ' . $this->db->quote($status))
						  ->where('`id` IN(' . implode(',', $ids) . ')');

			$this->db->setQuery($q);
			$this->db->execute();

			return true;
		}
		catch (Exception $e)
		{
			JFactory::getApplication()->enqueueMessage('DB error: ' . $e->getMessage(), 'error');

			return false;
		}
	}

	/**
	 * @param int    $id
	 * @param string $status
	 *
	 * @return bool
	 */
	public function updateItemStatus($id, $status = 'tracked')
	{
		$id = (int) $id;

		if (!(int) $id)
		{
			return true;
		}

		try
		{
			$row = (object) array(
				'id'     => $id,
				'status' => $status,
			);

			$this->db->updateObject('#__gtm_queue', $row, 'id');

			return true;
		}
		catch (Exception $e)
		{
			JFactory::getApplication()->enqueueMessage('DB error: ' . $e->getMessage(), 'error');

			return false;
		}
	}

	/**
	 *
	 * @param string $status
	 *
	 * @return array|false
	 */
	public function getItems($status = 'pending')
	{
		$q = $this->db->getQuery(true)
					  ->select('*')
					  ->from('#__gtm_queue');

		if ($status != '')
		{
			$q->where('status = ' . $this->db->quote($status));
		}

		$this->db->setQuery($q);

		return $this->db->loadObjectList();
	}

	/**
	 * @return bool
	 */
	public function deleteTrackedItems()
	{
		try
		{
			$q = $this->db->getQuery(true)
						  ->delete('#__gtm_queue')
						  ->where("status = 'tracked'");

			$this->db->setQuery($q);
			$this->db->execute();

			return true;
		}
		catch (Exception $e)
		{
			JFactory::getApplication()->enqueueMessage('DB error: ' . $e->getMessage(), 'error');

			return false;
		}
	}

	/**
	 * @return bool
	 */
	public function createTable()
	{
		try
		{
			$this->db->setQuery('CREATE TABLE IF NOT EXISTS `#__gtm_queue` ('
								. '`id` INT(11) NOT NULL AUTO_INCREMENT, '
								. '`status` VARCHAR(30) NOT NULL DEFAULT \'pending\', '
								. '`plugin` VARCHAR(30) NOT NULL, '
								. '`json` TEXT NOT NULL, '
								. '`created` DATETIME NOT NULL, '
								. 'PRIMARY KEY (`id`), '
								. 'KEY `status` (`status`), '
								. 'KEY `plugin` (`plugin`) '
								. ') ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;');
			$this->db->execute();

			return true;
		}
		catch (Exception $e)
		{
			JFactory::getApplication()->enqueueMessage('DB error: ' . $e->getMessage(), 'error');

			return false;
		}
	}

	/**
	 * @return bool
	 */
	public function truncateTable()
	{
		try
		{
			$this->db->setQuery("TRUNCATE TABLE `#__gtm_queue`");
			$this->db->execute();

			return true;
		}
		catch (Exception $e)
		{
			JFactory::getApplication()->enqueueMessage('DB error: ' . $e->getMessage(), 'error');

			return false;
		}
	}

	/**
	 * @return bool
	 */
	public function dropTable()
	{
		try
		{
			$db = JFactory::getDbo();

			$db->dropTable('#__gtm_queue');

			return true;
		}
		catch (Exception $e)
		{
			JFactory::getApplication()->enqueueMessage('DB error: ' . $e->getMessage(), 'error');

			return false;
		}
	}
}