<?php
/**
 *  @Copyright
 *  @package     JCC - JS CSS Control
 *  @author      Viktor Vogel {@link http://www.kubik-rubik.de}
 *  @version     2.5-5 - 2013-12-10
 *  @link        http://joomla-extensions.kubik-rubik.de/jcc-js-css-control
 *
 *  @license GNU/GPL
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
defined('_JEXEC') or die('Restricted access');

class PlgSystemJsCssControl extends JPlugin
{
    protected $_request;
    protected $_debug;
    protected $_debug_js = array();
    protected $_debug_css = array();
    protected $_exclude_js_files;
    protected $_exclude_css_files;
    protected $_excluded_files = array();

    function __construct(&$subject, $config)
    {
        // Not in administration
        $app = JFactory::getApplication();

        if($app->isAdmin())
        {
            return;
        }

        parent::__construct($subject, $config);
        $this->loadLanguage('plg_system_jscsscontrol', JPATH_ADMINISTRATOR);
        $this->set('_request', JFactory::getApplication()->input);
        $this->set('_debug', $this->params->get('debug', 0));
    }

    /**
     * Checks first time before the head is compiled
     */
    public function onBeforeCompileHead()
    {
        $js = $this->params->get('js');
        $css = $this->params->get('css');

        if(!empty($js) OR !empty($css))
        {
            $document = JFactory::getDocument();

            // Exclude JavaScript files
            if(!empty($js))
            {
                $this->_exclude_js_files = $this->getFilesToExclude($js);

                if(!empty($this->_exclude_js_files))
                {
                    $loaded_files_js = $document->_scripts;
                    $document->_scripts = $this->excludeFilesOnBeforeCompileHead($this->_exclude_js_files, $loaded_files_js);
                }
            }

            // Exclude CSS files
            if(!empty($css))
            {
                $this->_exclude_css_files = $this->getFilesToExclude($css);

                if(!empty($this->_exclude_css_files))
                {
                    $loaded_files_css = $document->_styleSheets;
                    $document->_styleSheets = $this->excludeFilesOnBeforeCompileHead($this->_exclude_css_files, $loaded_files_css);
                }
            }
        }

        // Debug mode
        if($this->_debug == true)
        {
            // Set the placeholder for the parameters example output
            JFactory::getApplication()->enqueueMessage('PLG_JSCSSCONTROL_DEBUGOUTPUTPLACEHOLDER');

            // Save all files which could be retrieved directly from the JDocument object
            if(!empty($loaded_files_js))
            {
                $this->_debug_js = array_keys($loaded_files_js);
            }

            if(!empty($loaded_files_css))
            {
                $this->_debug_css = array_keys($loaded_files_css);
            }

            $css = '.jcc-summary{font-weight: bolder;}'."\n";
            $css .= '.jcc-loaded{color: green;}'."\n";
            $css .= '.jcc-excluded{color: red;}';
            JFactory::getDocument()->addStyleDeclaration($css);
        }
    }

    /**
     * Checks the output in the trigger onAfterRender() to remove also files from the template and other extensions
     */
    public function onAfterRender()
    {
        if(!empty($this->_exclude_js_files) OR !empty($this->_exclude_css_files) OR !empty($this->_debug))
        {
            // Remove JS and CSS directly from the output
            $body = JResponse::getBody();

            if(!empty($this->_exclude_js_files))
            {
                preg_match_all('@<script[^>]*src=["|\']([^>]*\.js)(\?[^>]*)?["|\'][^>]*/?>.*</script>@isU', $body, $matches_js);
                $this->excludeFilesOnAfterRender($body, $this->_exclude_js_files, $matches_js);
            }

            if(!empty($this->_exclude_css_files))
            {
                preg_match_all('@<link[^>]*href=["|\']([^>]*\.css)(\?[^>]*)?["|\'][^>]*/?>@isU', $body, $matches_css);
                $this->excludeFilesOnAfterRender($body, $this->_exclude_css_files, $matches_css);
            }

            // Debug mode
            if($this->_debug == true)
            {
                $debug_output_parameters = $this->getDebugInformation();

                // Get information of all included files if they are not known already
                if(empty($matches_js[1]))
                {
                    preg_match_all('@<script[^>]*src=["|\']([^>]*\.js)(\?[^>]*)?["|\'][^>]*/?>.*</script>@isU', $body, $matches_js);
                }

                $debug_js_files = array_unique(array_merge($this->_debug_js, $matches_js[1]));

                if(empty($matches_css[1]))
                {
                    preg_match_all('@<link[^>]*href=["|\']([^>]*\.css)(\?[^>]*)?["|\'][^>]*/?>@isU', $body, $matches_css);
                }

                $debug_css_files = array_unique(array_merge($this->_debug_css, $matches_css[1]));

                if(!empty($debug_js_files) OR !empty($debug_css_files))
                {
                    $debug_output_example = '';

                    if(!empty($debug_js_files))
                    {
                        $debug_output_example .= $this->createDebugParametersOutput($debug_js_files, $debug_output_parameters, 'js');
                    }

                    if(!empty($debug_css_files))
                    {
                        $debug_output_example .= $this->createDebugParametersOutput($debug_css_files, $debug_output_parameters, 'css');
                    }
                }
                else
                {
                    $debug_output_example = JTEXT::_('PLG_JSCSSCONTROL_DEBUGOUTPUT_NOFILES');
                }

                $body = str_replace('PLG_JSCSSCONTROL_DEBUGOUTPUTPLACEHOLDER', JTEXT::sprintf('PLG_JSCSSCONTROL_DEBUGOUTPUT', $debug_output_parameters, $debug_output_example), $body);
            }

            JResponse::setBody($body);
        }
    }

    /**
     * Gets all files which have to be excluded on the loaded page
     *
     * @param array $type
     * @return array
     */
    private function getFilesToExclude($type)
    {
        $exclude_files = array();

        $params = array_map('trim', explode("\n", $type));
        $lines = array();

        foreach($params as $params_line)
        {
            $lines[] = array_map('trim', explode('|', $params_line));
        }

        foreach($lines as $line)
        {
            $exclude_file = true;

            if(isset($line[1]))
            {
                $parameters = $line[1];
            }

            if(!empty($parameters))
            {
                $parameters = array_map('trim', explode(',', $parameters));

                foreach($parameters as $parameter)
                {
                    $parameter = array_map('trim', explode('=', $parameter));

                    $exclude_file = $this->checkParameters($parameter);

                    if($exclude_file == false)
                    {
                        break;
                    }
                }
            }

            if($exclude_file == true)
            {
                $exclude_files[] = $line[0];
            }

            unset($parameters);
        }

        return $exclude_files;
    }

    /**
     * Excludes the files from being loaded in the browser - OnBeforeCompileHead
     *
     * @param array $exclude_files
     * @param array $loaded_files
     */
    private function excludeFilesOnBeforeCompileHead(&$exclude_files, $loaded_files)
    {
        $loaded_files_keys = array_keys($loaded_files);

        foreach($loaded_files_keys as $loaded_file)
        {
            foreach($exclude_files as $exclude_file)
            {
                if(preg_match('@'.preg_quote($exclude_file).'@', $loaded_file))
                {
                    unset($loaded_files[$loaded_file]);
                    unset($exclude_files[array_search($exclude_file, $exclude_files)]);
                    $this->_excluded_files[] = $exclude_file;
                    break;
                }
            }
        }

        return $loaded_files;
    }

    /**
     * Excludes the files from being loaded in the browser - OnAfterRender
     *
     * @param string $body
     * @param array $exclude_files
     * @param array $matches
     */
    private function excludeFilesOnAfterRender(&$body, &$exclude_files, $matches)
    {
        foreach($matches[1] as $key => $match)
        {
            foreach($exclude_files as $exclude_file)
            {
                if(preg_match('@'.preg_quote($exclude_file).'@', $match))
                {
                    $body = str_replace($matches[0][$key], '', $body);
                    unset($exclude_files[array_search($exclude_file, $exclude_files)]);
                    $this->_excluded_files[] = $exclude_file;
                    break;
                }
            }
        }
    }

    /**
     * Checks entered parameters if they are loaded on the specific URL
     *
     * @param string $parameter
     * @return boolean
     */
    private function checkParameters($parameter)
    {
        $name = $this->_request->get($parameter[0], array(0), 'array');

        if($name[0] == $parameter[1])
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Generates information output for debug mode
     *
     * @return type
     */
    private function getDebugInformation()
    {
        $debug_array = array();
        $debug_output = array();

        // Get all important request variables to identify the page
        $debug_array['option'] = $this->_request->getWord('option');
        $debug_array['view'] = $this->_request->getWord('view');
        $debug_array['task'] = $this->_request->getCmd('task');
        $debug_array['func'] = $this->_request->getWord('func');
        $debug_array['layout'] = $this->_request->getWord('layout');
        $debug_array['Itemid'] = $this->_request->getCmd('Itemid');
        $debug_array['id'] = $this->_request->getString('id'); // ID can contain the alias - ID:ALIAS

        $debug_array = array_filter($debug_array);

        foreach($debug_array as $key => $value)
        {
            if(!empty($value))
            {
                $debug_output[] = $key.'='.$value;
            }
        }

        return implode(',', $debug_output);
    }

    /**
     * Creates the output with all files and sizes for the debug mode
     *
     * @param array $debug_files
     * @param string $debug_output_parameters
     * @param string $type
     * @return string
     */
    private function createDebugParametersOutput($debug_files, $debug_output_parameters, $type)
    {
        $debug_output_array = array();
        $size_total = 0;
        $size_loaded = 0;
        $size_excluded = 0;

        foreach($debug_files as $debug_file)
        {
            // Check and adjust the path to the file to get the correct size
            if(strpos($debug_file, JURI::base()) !== false)
            {
                $debug_file_path = str_replace(JURI::base(), '', $debug_file);
            }
            elseif(strpos($debug_file, JURI::base(true)) !== false)
            {
                $debug_file_path = str_replace(JURI::base(true), '', $debug_file);
            }
            else
            {
                $debug_file_path = $debug_file;
            }

            if(substr($debug_file_path, 0, 1) != '/' AND substr($debug_file_path, 0, 4) != 'http')
            {
                $debug_file_path = '/'.$debug_file_path;
            }

            if(substr($debug_file_path, 0, 4) != 'http')
            {
                $size_raw = @filesize(JPATH_BASE.$debug_file_path) / 1024;
            }
            else
            {
                // Do not handle external files
                $size_raw = 0;
            }

            $size_total += $size_raw;

            if(!in_array($debug_file, $this->_excluded_files))
            {
                $debug_output_array[] = '<span class="jcc-loaded">'.$debug_file.'|'.$debug_output_parameters.' '.$this->formatSizeKb($size_raw).'</span>';
                $size_loaded += $size_raw;
            }
            else
            {
                $debug_output_array[] = '<span class="jcc-excluded">'.$debug_file.'|'.$debug_output_parameters.' '.$this->formatSizeKb($size_raw).'</span>';
                $size_excluded += $size_raw;
                unset($this->_excluded_files[array_search($debug_file, $this->_excluded_files)]);
            }
        }

        return '<p class="jcc-summary">'.JTEXT::sprintf('PLG_JSCSSCONTROL_DEBUGOUTPUT_SUMMARY', strtoupper($type), count($debug_output_array), $this->formatSizeKb($size_total), '<span class="jcc-excluded">'.$this->formatSizeKb($size_excluded).'</span>', '<span class="jcc-loaded">'.$this->formatSizeKb($size_loaded).'</span>').'</p><pre><code>'.implode('<br />', $debug_output_array).'</code></pre>';
    }

    /**
     * Formats the size to 2 decimals and add string _KB
     *
     * @param float $size
     * @return string
     */
    private function formatSizeKb($size)
    {
        return number_format($size, 2, ',', '.').' KB';
    }

}
