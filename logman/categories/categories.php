<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * Categories LOGman Plugin.
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Plugin\LOGman
 */
class PlgLogmanCategories extends ComLogmanPluginJoomla
{
    protected function _initialize(KObjectConfig $config)
    {
        $config->append(array(
            'resources' => array('category')
        ));

        parent::_initialize($config);
    }

    protected function _getCategoryObjectData($data, $event)
    {
        return array('id' => $data->id, 'name' => $data->title, 'metadata' => array('extension' => $data->extension));
    }
}