<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * Modules LOGman Plugin.
 *
 * Provides event handlers for dealing with com_modules events.
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Plugin\LOGman
 */
class PlgLogmanModules extends ComLogmanPluginJoomla
{
    protected function _initialize(KObjectConfig $config)
    {
        $config->append(array(
            'resources' => array('module')
        ));

        parent::_initialize($config);
    }
}