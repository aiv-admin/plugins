<?php



defined ('_JEXEC') or die('Restricted access');



/**

*  This Virtuemart shipping plugin is used with a production UPS Shipping Account

*  to obtain the proper shipping rates and options from UPS using the weight of the

*  products in the cart. 

*

* @version v1.2.1 2014/11/26 by Park Beach Systems, Inc.

* @package VirtueMart

* @subpackage shipping

* @copyright Copyright (C) 2013-2014 Park Beach Systems, Inc. All rights reserved.

* @license GNU General Public License version 3, or later http://www.gnu.org/licenses/gpl.html

*/

if (!class_exists ('vmPSPlugin')) {

	require(JPATH_VM_PLUGINS . DS . 'vmpsplugin.php');

}

/**

* This is the Shipping class to call the UPS API for shipping costs

*/



class plgVmShipmentUPS_ship extends vmPSPlugin {

	var $ups_username;

	var $ups_password;

	var $ups_accesskey;

	var $ups_accountnumber;

	var $ups_server;

	var $ups_path;

	var $ups_proxyserver;

	var $ups_reporterrors;

	var $ups_customerclassification;

	var $ups_pickuptype;

	var $ups_packagingtype;

	var $ups_residentialindicator;

	var $ups_padding;

	var $ups_packagesize;

	var $ups_declaredvalue;

	var $ups_deliveryconfirmation;

	var $ups_weightunit;

	var $ship_service = array();

	var $ship_postage = array();

	var $shipmethod_rate = array(); //stores rates by shipmethodid

	//var $ups_errored = false;

	var $ups_api_status = array();

	var $ups_residentialaddress = true;

	var $vendoraddress = array();

	

	// instance of class

	public static $_this = FALSE;



	/**

	 * @param object $subject

	 * @param array  $config

	 */

	function __construct (& $subject, $config) {

		//vmdebug('__construct');

		parent::__construct ($subject, $config);

			

		$this->_loggable = TRUE;

		$this->_tablepkey = 'id';

		$this->_tableId = 'id';

		$this->tableFields = array_keys ($this->getTableSQLFields ());

		$varsToPush = $this->getVarsToPush ();

		$this->setConfigParameterable ($this->_configTableFieldName, $varsToPush);

		$this->_debug = $this->params->get( 'debug', 0);

		$this->ups_username = $this->params->get( 'UPS_USERNAME', '' );

		$this->ups_password = $this->params->get( 'UPS_PASSWORD', '' );

		$this->ups_accesskey = $this->params->get( 'UPS_ACCESSKEY', '' );

		$this->ups_accountnumber = $this->params->get( 'UPS_ACCOUNTNUMBER', '' );

		$this->ups_server = $this->params->get( 'UPS_SERVER', '' ); 

		//$this->ups_path = $this->params->get( 'UPS_PATH', '' );

		$this->ups_path = '/ups.app/xml/Rate';

		$this->ups_proxyserver = $this->params->get( 'UPS_PROXYSERVER', '' ); //"http://proxy.shr.secureserver.net:3128";

		$this->ups_reporterrors = $this->params->get( 'UPS_REPORTERRORS', 1 );

		$this->ups_padding = $this->params->get( 'UPS_PADDING', '' );

		$this->ups_customerclassification = $this->params->get( 'UPS_CUSTOMERCLASSIFICATION', '00' );

		$this->ups_pickuptype = $this->params->get( 'UPS_PICKUPTYPE', '03' );

		$this->ups_packagingtype = $this->params->get( 'UPS_PACKAGINGTYPE', '00' );			

		$this->ups_residentialindicator = $this->params->get( 'UPS_RESIDENTIALINDICATOR', '0' );

		$this->ups_declaredvalue = $this->params->get( 'UPS_DECLAREDVALUE', '0' );

		$this->ups_deliveryconfirmation = $this->params->get( 'UPS_DELIVERYCONFIRMATION', '0' );

		$this->ups_weightunit = $this->params->get( 'UPS_WEIGHTUNIT', 'LB' );

	}

	/**

	* Create UPS shipment table for this plugin

	*/

	public function getVmPluginCreateTableSQL () {

		

		return $this->createTableSQL ('Shipment UPS Table');

	}



	/**

	 * @return array

	 */

	function getTableSQLFields () {

		$SQLfields = array(

			'id'                           => 'int(1) UNSIGNED NOT NULL AUTO_INCREMENT',

			'virtuemart_order_id'          => 'int(11) UNSIGNED',

			'order_number'                 => 'char(32)',

			'virtuemart_shipmentmethod_id' => 'mediumint(1) UNSIGNED',

			'shipment_name'                => 'varchar(5000)',

			'order_weight'                 => 'decimal(10,4)',

			'shipment_weight_unit'         => 'char(3) DEFAULT \'LB\'',

			'shipment_cost'                => 'decimal(10,2)',

			'shipment_package_fee'         => 'decimal(10,2)',

			'tax_id'                       => 'smallint(1)'

		);

		return $SQLfields;

	}



	/**

	 * This method is fired when showing the order details in the frontend.

	 * It displays the shipment-specific data.

	 */

	public function plgVmOnShowOrderFEShipment ($virtuemart_order_id, $virtuemart_shipmentmethod_id, &$shipment_name) {

		//vmdebug('plgVmOnShowOrderFEShipment');

		$this->onShowOrderFE ($virtuemart_order_id, $virtuemart_shipmentmethod_id, $shipment_name);

	}



	/**

	 * This event is fired after the order has been stored; it gets the shipment method-

	 * specific data.

	 */

	function plgVmConfirmedOrder (VirtueMartCart $cart, $order) {

		//vmdebug('ConfirmedOrder');

		if (!($method = $this->getVmPluginMethod ($order['details']['BT']->virtuemart_shipmentmethod_id))) {

			return NULL; // Another method was selected, do nothing

		}

		if (!$this->selectedThisElement ($method->shipment_element)) {

			return FALSE;

		}

		

		$methodSalesPrice = $_SESSION['cart']['ups_ship_rate'];

		$_SESSION['shipTotall'] = $_SESSION['cart']['ups_ship_rate']; // stores shipment cost to session

		$methodHandlingFee = $this->_getHandlingCost($method, $methodSalesPrice, $cart->pricesUnformatted['salesPrice']);

		

		$values['virtuemart_order_id'] = $order['details']['BT']->virtuemart_order_id;

		$values['order_number'] = $order['details']['BT']->order_number;

		$values['virtuemart_shipmentmethod_id'] = $order['details']['BT']->virtuemart_shipmentmethod_id;

		$values['shipment_name'] = $this->renderPluginName ($method);

		//$values['order_weight'] = $this->getOrderWeight ($cart, 'LB');

		$values['order_weight'] = $this->getOrderWeight ($cart, $this->ups_weightunit);

		//$values['shipment_weight_unit'] = 'LB';	

		$values['shipment_weight_unit'] = $this->ups_weightunit;

		$values['shipment_cost'] = $methodSalesPrice;

		$values['shipment_package_fee'] = $methodHandlingFee;

		$values['tax_id'] = $method->tax_id;

		$this->storePSPluginInternalData ($values);



		return TRUE;

	}



	/**

	 * This method is fired when showing the order details in the backend.

	 * It displays the shipment-specific data.

	 */

	public function plgVmOnShowOrderBEShipment ($virtuemart_order_id, $virtuemart_shipmentmethod_id) {

		//vmdebug('plgVmOnShowOrderBEShipment');

		if (!($this->selectedThisByMethodId ($virtuemart_shipmentmethod_id))) {

			return NULL;

		}

		$html = $this->getOrderShipmentHtml ($virtuemart_order_id);

		return $html;

	}



	/**

	 * This method displays the shipping data stored in the method's table.

	 */

	function getOrderShipmentHtml ($virtuemart_order_id) {

		//vmdebug('getOrderShipmentHtml');

		$db = JFactory::getDBO ();

		$q = 'SELECT * FROM `' . $this->_tablename . '` '

			. 'WHERE `virtuemart_order_id` = ' . $virtuemart_order_id;

		$db->setQuery ($q);

		if (!($shipinfo = $db->loadObject ())) {

			vmWarn (500, $q . " " . $db->getErrorMsg ());

			return '';

		}



		if (!class_exists ('CurrencyDisplay')) {

			require(JPATH_VM_ADMINISTRATOR . DS . 'helpers' . DS . 'currencydisplay.php');

		}



		$currency = CurrencyDisplay::getInstance ();

		$tax = ShopFunctions::getTaxByID ($shipinfo->tax_id);

		$taxDisplay = is_array ($tax) ? $tax['calc_value'] . ' ' . $tax['calc_value_mathop'] : $shipinfo->tax_id;

		$taxDisplay = ($taxDisplay == -1) ? JText::_ ('COM_VIRTUEMART_PRODUCT_TAX_NONE') : $taxDisplay;

			

		$html = '<table class="adminlist">' . "\n";

		$html .= $this->getHtmlHeaderBE ();

		$html .= $this->getHtmlRowBE ('UPS_SHIP_SHIPPING_NAME', $shipinfo->shipment_name);

		$html .= $this->getHtmlRowBE ('UPS_SHIP_WEIGHT', $shipinfo->order_weight . ' ' . ShopFunctions::renderWeightUnit ($shipinfo->shipment_weight_unit));

		$html .= $this->getHtmlRowBE ('UPS_SHIP_HANDLING_FEE_APPLIED', $currency->priceDisplay ($shipinfo->shipment_package_fee));

		$html .= $this->getHtmlRowBE ('UPS_SHIP_COST', $currency->priceDisplay ($shipinfo->shipment_cost));

		$html .= $this->getHtmlRowBE ('UPS_SHIP_TAX', $taxDisplay);

		$html .= '</table>' . "\n";



		return $html;

	}



	/**

	 * This method returns the shipping cost for the selected method.

	 */

	function getCosts (VirtueMartCart $cart, $method, $cart_prices) {

		$pluginmethod_id = $this->_idName;

		$methodSalesPrice = $this->shipmethod_rate[$method->$pluginmethod_id];

		$_SESSION['cart']['ups_ship_rate'] = $methodSalesPrice;

		$this->logInfo('**Setting UPS cost for shipment_rate'.$method->$pluginmethod_id .'='.$this->shipmethod_rate[$method->$pluginmethod_id]);



		return $methodSalesPrice;

	}

	/**

	* This method determines the handing costs for the shipping method.

	* @param                $method

	* @param                $methodSalesPrice

	* @return currency

	*/

	function _getHandlingCost($method, $methodSalesPrice, $cartvalue) {

		//vmdebug('getHandlingCost '.$method->UPS_HANDLINGFEE_TYPE.'= '.$method->UPS_HANDLINGFEE);

		if($method->UPS_HANDLINGFEE_TYPE == 'NONE'){

			//No handling fee applied

			return 0;

		}else{

			if (preg_match('/%$/',$method->UPS_HANDLINGFEE)) {

				if($method->UPS_HANDLINGFEE_TYPE == 'PCTCART'){

					//Add percentage based on cart total

					return $cartvalue * (substr($method->UPS_HANDLINGFEE,0,-1)/100);

				}else{ //Add percentage based on shipping method total

					return $methodSalesPrice * (substr($method->UPS_HANDLINGFEE,0,-1)/100);

				}

			} else {

				return $method->UPS_HANDLINGFEE;

			}

		}

	}

	/**

	* This method sets the vendor's address arrat for UPS for shipping.

	* @param	$vendorid

	* @return	address array

	*/

	function _getVendorAddress($vendorId) {

		$vendorModel = VmModel::getModel ('vendor');

		$vendorAddress = $vendorModel->getVendorAdressBT($vendorId);

		if ($vendorAddress == NULL){

			vmWarn('Error obtaining vendor zipcode from shop settings.');

			return;

		}

		return $vendorAddress;

	}

	/**

	* This method determines if a call to UPS API is required based on changes in cart.

	* @param VirtueMartCart $cart

	* @param $method

	* @return int

	*/

	function _requireUPSUpdate($cart) {

		$this->logInfo('++requireUPSUpdate');

		$requireupdate = false;

		//Check cart weight

		$orderWeight = $this->getOrderWeight ($cart, 'LB');

		$this->logInfo('  orderWeight'.$orderWeight.' - '.$_SESSION['cart']['ups_ship_weight']);

		if ($orderWeight !== $_SESSION['cart']['ups_ship_weight']){

			$this->logInfo('  Cart weight changed');

			$requireupdate = true;

		}		

		//Check source zip

		$vendoraddress = $this->_getVendorAddress($cart->vendorId);

		if ($vendoraddress->zip != $_SESSION['cart']['ups_ship_source_zip']){

			$this->logInfo('  Shipping source changed');

			$requireupdate = true;

		}

		//Check destination zip		

		$address = (($cart->ST == 0) ? $cart->BT : $cart->ST);

		if ($address['zip'] != $_SESSION['cart']['ups_ship_dest_zip']){

			$this->logInfo('  Shipping destination changed');

			$requireupdate = true;

		}



		return $requireupdate;

	}

	/**

	 * @param \VirtueMartCart $cart

	 * @param int             $method

	 * @param array           $cart_prices

	 * @return bool

	 */

	protected function checkConditions ($cart, $method, $cart_prices) {

		$this->logInfo('++checkConditions: '.trim($method->shipment_name).' ('.trim($method->UPS_SERVICE).')');

		

		$ups_cond = false;

		$this->convert ($method);

		$pluginmethod_id = $this->_idName;

		

		$orderWeight = $this->getOrderWeight ($cart, 'LB');

		$orderWeightcond = true;

		if ($orderWeight <= 0) {

			$this->logInfo('  FALSE Reason: Cart weight is '.$orderWeight);

			$orderWeightcond = false;

		}

		if($orderWeight > 150.00 ) {

			$this->logInfo('  FALSE Reason: Cart weight of '.$orderWeight.' pounds exceeds the 150 pound limit.');

			$orderWeightcond = false;

		}

		$address = (($cart->ST == 0) ? $cart->BT : $cart->ST);

		$type = (($cart->ST == 0) ? 'BT' : 'ST');

		$countries = array();

		if (!empty($method->countries)) {

			if (!is_array ($method->countries)) {

				$countries[0] = $method->countries;

			} else {

				$countries = $method->countries;

			}

		}

		// probably did not give his BT:ST address

		if (!is_array ($address)) {

			$address = array();

			$address['zip'] = 0;

			$address['virtuemart_country_id'] = 0;

		}

		if(isset($cart_prices['salesPrice'])){

			$orderamount_cond = $this->testRange($cart_prices['salesPrice'],$method,'orderamount_start','orderamount_stop','order amount');

		} else {

			$orderamount_cond = FALSE;

		}



		$userFieldsModel =VmModel::getModel('Userfields');



		if (isset($address['zip'])) {

			$zip_cond = $this->testRange($address['zip'],$method,'zip_start','zip_stop','zip');

		} else {

	

			$zip_cond = true;

		}



		if (!isset($address['virtuemart_country_id'])) {

			$address['virtuemart_country_id'] = 0;

		}

		if (in_array ($address['virtuemart_country_id'], $countries) || count ($countries) == 0) {

			$country_cond = true;

		}else{

			$this->logInfo('  FALSE for variable virtuemart_country_id = '.implode($countries,', ').', Reason: Country does not fit');

			$country_cond = false;

		}

	

		//2/18/2014 - Support for UPS SurePost Rates

		switch (trim($method->UPS_SERVICE)){

		case "92":

			//UPS SurePost® Less than 1 lb – for parcels with a weight of 1 ounce to 15.99 ounces

			$ups_service_code = '92';

			if ($orderWeight >= 1) {

				$this->logInfo('  FALSE Reason: Cart weight is '.$orderWeight);

				$orderWeightcond = false;

			}

			break;

		case "93":

			//UPS SurePost®  1 lb or Greater – for parcels with a weight of 1 lb to 70 lbs

			$ups_service_code = '93';

			if ($orderWeight < 1 || $orderWeight > 70) {

				$this->logInfo('  FALSE Reason: Cart weight is '.$orderWeight);

				$orderWeightcond = false;

			}

			break;

		case "94":

			//UPS SurePost® BPM – a content driven service for bound printed matter parcels with weight of 1 lb to 15 lbs

			$ups_service_code = '94';

			if ($orderWeight < 1 || $orderWeight > 15) {

				$this->logInfo('  FALSE Reason: Cart weight is '.$orderWeight);

				$orderWeightcond = false;

			}

			break;

		case "95":

			//UPS SurePost® Media – a content driven service for media parcels with weight of 1 lb to 70 lbs

			$ups_service_code = '95';

			if ($orderWeight < 1 || $orderWeight > 70) {

				$this->logInfo('  FALSE Reason: Cart weight is '.$orderWeight);

				$orderWeightcond = false;

			}

			break;

		default:

			//do not allow PO Box delivery outside of SurePost

			if(preg_match("/^\s*((P(OST)?.?\s*O(FF(ICE)?)?.?\s+(B(IN|OX))?)|B(IN|OX))/i", $address['address_1'])) {

				$this->logInfo('  FALSE Reason: Address is a PO Box. '.$address['address_1']);

				$zip_cond = false;

			}

			$ups_service_code = 'All';

		}

		

		$allconditions = (int)$orderWeightcond + (int)$zip_cond + (int)$country_cond + (int)$orderamount_cond;

		if($allconditions === 4){

			//Continue with UPS API call

			//2/18/2014 - Trigger call for UPS SurePost Rates every time since unique request

			//if (($this->ship_service == null) && ($this->ups_errored == false)){ //do not call if already called

			//if ((($this->ship_service == null) && ($this->ups_errored == false)) || ($ups_service_code !== 'All')){ //do not call if already called

			//if ((($this->ship_service == null) && ($this->ups_api_status($ups_service_code) !== 'error')) || ($ups_service_code !== 'All' && $this->shipmethod_rate[$method->$pluginmethod_id] == null)){ //do not call if already called

			if ($this->ups_api_status[$ups_service_code] == null){

				//$this->_getUPSRates($orderWeight, $cart, $address, $cart_prices['salesPrice']);

				$this->_getUPSRates($orderWeight, $cart, $address, $cart_prices['salesPrice'], $ups_service_code);

			}					

			$count = count($this->ship_service);

			if ($count < 1){

				$this->logInfo('  No UPS options returned from UPS service.');

			}else{

				$this->logInfo('  UPS service returned '.$count.' possible mail service options.');

				$i = 0;

				while ($i < $count) {

					// UPS returns Charges in USD.

					if ($this->ship_service[$i] == $method->UPS_SERVICE){

						//set rate of method as ups_ship_rate to support autoselect when one shipping method exists

						//vmdebug('Cart prices:'.$cart->pricesUnformatted['SalesPrice']);

						$methodSalesPrice = floatval($this->ship_postage[$i]);

						$methodSalesPrice = $methodSalesPrice + $this->_getHandlingCost($method, $methodSalesPrice, $cart_prices['salesPrice']);

						$this->shipmethod_rate[$method->$pluginmethod_id] = $methodSalesPrice;

						$this->logInfo('  Setting UPS cost for shipment_rate'.$method->$pluginmethod_id.' = '.$this->shipmethod_rate[$method->$pluginmethod_id]);

						$ups_cond = true;

						break;

					}

					$i++;

				}

				if(!$ups_cond){

					$this->logInfo('  '.$method->shipment_name.' not matched with returned services.'.$cart->virtuemart_shipmentmethod_id);

				}

			}

		}

		if($ups_cond){

			$this->logInfo('  '.$method->shipment_name.' DOES apply for this cart.');

			return TRUE;

		}else{

			$this->logInfo('  '.$method->shipment_name.' DOES NOT apply for this cart.');

			//if this shipping method is currently selected but fails condition we want to remove it.

			//this is required because we do not recall 'checkconditions' when setting price on CART page.

			if($method->$pluginmethod_id == $cart->virtuemart_shipmentmethod_id){

				//vmdebug(' MATCH: method_id='.$method->$pluginmethod_id.' virtuemart_shipmentmethod_id='.$cart->virtuemart_shipmentmethod_id);

				$cart->virtuemart_shipmentmethod_id = null;

			}

			return FALSE;

		}

	}



	/**

	 * @param $method

	 */

	function convert (&$method) {



		$method->orderamount_start = (float)$method->orderamount_start;

		$method->orderamount_stop = (float)$method->orderamount_stop;

		$method->zip_start = (int)$method->zip_start;

		$method->zip_stop = (int)$method->zip_stop;

	

	}



	private function testRange($value, $method, $floor, $ceiling,$name){



		$cond = true;

		if(!empty($method->$floor) and !empty($method->$ceiling)){

			$cond = (($value >= $method->$floor AND $value <= $method->$ceiling));

			if(!$cond){

				$result = 'FALSE';

				$reason = 'is NOT within Range of the condition from '.$method->$floor.' to '.$method->$ceiling;

			} else {

				$result = 'TRUE';

				$reason = 'is within Range of the condition from '.$method->$floor.' to '.$method->$ceiling;

			}

		} else if(!empty($method->$floor)){

			$cond = ($value >= $method->$floor);

			if(!$cond){

				$result = 'FALSE';

				$reason = 'is not at least '.$method->$floor;

			} else {

				$result = 'TRUE';

				$reason = 'is over min limit '.$method->$floor;

			}

		} else if(!empty($method->$ceiling)){

			$cond = ($value <= $method->$ceiling);

			if(!$cond){

				$result = 'FALSE';

				$reason = 'is over '.$method->$ceiling;

			} else {

				$result = 'TRUE';

				$reason = 'is lower than the set '.$method->$ceiling;

			}

		} else {

			$result = 'TRUE';

			$reason = 'no boundary conditions set';

		}

		if($result == 'FALSE'){

			$this->logInfo(' '.$method->shipment_name.' = '.$result.' for variable '.$name.' = '.$value.' Reason: '.$reason);

		}

		return $cond;

	}



	/**

	 * Create the table for this plugin if it does not yet exist.

	 */

	function plgVmOnStoreInstallShipmentPluginTable ($jplugin_id) {



		return $this->onStoreInstallPluginTable ($jplugin_id);

	}



	/**

	 */

	public function plgVmOnSelectCheckShipment (VirtueMartCart &$cart) {

		//vmdebug('plgVmOnSelectCheckShipment');

		$_SESSION['cart']['ups_ship_rate'] = str_replace (" ", "", JRequest::getVar ('shipment_id_' . $cart->virtuemart_shipmentmethod_id.'_rate', ''));					

		return $this->OnSelectCheck ($cart);

	}



	/**

	 * This event is fired to display the pluginmethods in the cart (edit shipment/payment) for example

	 */

	public function plgVmDisplayListFEShipment (VirtueMartCart $cart, $selected = 0, &$htmlIn) {



		return $this->displayListFE ($cart, $selected, $htmlIn);

	}

	/**

	* displayListFE

	* This event handles all UPS options

	*

	* @param object  $cart Cart object

	* @param integer $selected ID of the method selected

	* @return boolean True on success, false on failures, null when this plugin was not selected.

	*/

	function displayListFE (VirtueMartCart $cart, $selected = 0, &$htmlIn) {

		//vmdebug('displayListFE');

		if ($this->getPluginMethods ($cart->vendorId) === 0) {

			if (empty($this->_name)) {

				vmAdminInfo ('displayListFE cartVendorId=' . $cart->vendorId);

				$app = JFactory::getApplication ();

				$app->enqueueMessage (JText::_ ('COM_VIRTUEMART_CART_NO_' . strtoupper ($this->_psType)));

				return FALSE;

			} else {

				return FALSE;

			}

		}

		

		$html = array();

		$method_name = $this->_psType . '_name';

		//clear ups session variables to trigger UPS API call

		$_SESSION['cart']['ups_ship_weight'] = 0;



		//Loop through each UPS shipping option in store to find any matches

		foreach ($this->methods as $method) {

			if ($this->checkConditions ($cart, $method, $cart->pricesUnformatted)) {

				$method->$method_name = $this->renderPluginName ($method);

				//Write out the shipping options

				$count = count($this->ship_service);

				if ($count < 1){

					$this->logInfo('  No UPS Options');

				}else{

					$i = 0;

					while ($i < $count) {

						// UPS returns Charges in USD.

						if ($this->ship_service[$i] == $method->UPS_SERVICE){

							$methodSalesPrice = floatval($this->ship_postage[$i]);

							$this->logInfo('  UPS service cost = '.$methodSalesPrice);					

							$methodSalesPrice = $methodSalesPrice + $this->_getHandlingCost($method, $methodSalesPrice, $cart->pricesUnformatted['salesPrice']);

							$this->logInfo('  UPS service cost after handling fee = '.$methodSalesPrice);

							$html [] = $this->getPluginHtml ($method, $selected, $methodSalesPrice);

							break; 

						}

						$i++;

					}

				}

			}

		}



		if (!empty($html)) {

			$htmlIn[] = $html;

			return TRUE;

		}

	

		return FALSE;

	}

	/**

	* Override in writing HTML for UPS shipping options

	*/

	protected function getPluginHtml ($plugin, $selectedPlugin, $pluginSalesPrice) {

		//vmdebug('getPluginHtml');

		$pluginmethod_id = $this->_idName;

		$pluginName = $this->_psType . '_name';

				

		if ($selectedPlugin == $plugin->$pluginmethod_id) {

			$checked = 'checked="checked"';

		} else {

			$checked = '';

		}

		

		if (!class_exists ('CurrencyDisplay')) {

			require(JPATH_VM_ADMINISTRATOR . DS . 'helpers' . DS . 'currencydisplay.php');

		}

		$currency = CurrencyDisplay::getInstance ();

		$costDisplay = "";

		if ($pluginSalesPrice) {

			$costDisplay = $currency->priceDisplay ($pluginSalesPrice);

			$costDisplay = '<span class="' . $this->_type . '_cost"> ' . JText::_ ('COM_VIRTUEMART_PLUGIN_COST_DISPLAY') . $costDisplay . "</span>";

		}



		$html = '<input type="radio" name="' . $pluginmethod_id . '" id="' . $this->_psType . '_id_' . $plugin->$pluginmethod_id . '"   value="' . $plugin->$pluginmethod_id . '" ' . $checked . ">\n"

		. '<label for="' . $this->_psType . '_id_' . $plugin->$pluginmethod_id . '">' . '<span class="' . $this->_type . '">' . $plugin->$pluginName . $costDisplay . "</span></label>\n"

		. '<input type="hidden" name="' . $this->_psType . '_id_' . $plugin->$pluginmethod_id . '_rate"   value="' . $pluginSalesPrice . '">';

		

		return $html;

	}

	/**

	 * @param VirtueMartCart $cart

	 * @param array          $cart_prices

	 * @param                $cart_prices_name

	 * @return bool|null

	 */

	public function plgVmOnSelectedCalculatePriceShipment (VirtueMartCart $cart, array &$cart_prices, &$cart_prices_name) {

		//vmdebug('plgVmOnSelectedCalculatePriceShipment');

		$id = $this->_idName;

		if (!($method = $this->selectedThisByMethodId ($cart->$id))) {

			return NULL; // Another method was selected, do nothing

		}

		if (!($method = $this->getVmPluginMethod ($cart->$id))) {

			return NULL;

		}



		$cart_prices_name = '';

		$cart_prices['cost'] = 0;



		//If there is no change to the cart that would require a UPS rate change then do not call 'checkConditions'

		if(!$this->_requireUPSUpdate($cart) && !empty($_SESSION['cart']['ups_ship_rate'])){

			$pluginmethod_id = $this->_idName;

			//If there has not been a rate set from this procedure then set rate to current cart UPS rate

			if(empty($this->shipmethod_rate[$method->$pluginmethod_id])){

				$this->shipmethod_rate[$method->$pluginmethod_id] = $_SESSION['cart']['ups_ship_rate'];				

			}

		}else{

			if (!$this->checkConditions ($cart, $method, $cart_prices)) {

				return FALSE;

			}			

		}



		$paramsName = $this->_psType . '_params';

		$cart_prices_name = $this->renderPluginName ($method);

		

		$this->setCartPrices ($cart, $cart_prices, $method);

		

		return TRUE;

		//return $this->onSelectedCalculatePrice ($cart, $cart_prices, $cart_prices_name);

	}

	



	/**

	 * Checks how many plugins are available. If only one, the user will not have the choice. Enter edit_xxx page

	 * The plugin must check first if it is the correct type

	 */

	function plgVmOnCheckAutomaticSelectedShipment (VirtueMartCart $cart, array $cart_prices = array(), &$shipCounter) {

		//vmdebug('OnCheckAutomaticSelected'.$shipCounter);

		if ($shipCounter > 1) {

			return 0;

		}



		return $this->onCheckAutomaticSelected ($cart, $cart_prices, $shipCounter);

	}



	function plgVmonShowOrderPrint ($order_number, $method_id) {

		//vmdebug('plgVmonShowOrderPrint');



		return $this->onShowOrderPrint ($order_number, $method_id);

	}



	function plgVmDeclarePluginParamsShipment ($name, $id, &$data) {

		//vmdebug('plgVmDeclarePluginParamsShipment');



		return $this->declarePluginParams ('shipment', $name, $id, $data);

	}

		

	function plgVmDeclarePluginParamsShipmentVM3 (&$data) {

		return $this->declarePluginParams ('shipment', $data);

	}



	/**

	 *  Validate data for plugin shipment in BE

	 */

	function plgVmSetOnTablePluginShipment(&$data,&$table){

		//vmdebug('plgVmSetOnTablePluginShipment');	

		$name = $data['shipment_element'];

		$id = $data['shipment_jplugin_id'];

		

		$handlingType = $data['UPS_HANDLINGFEE_TYPE'];



		if (!empty($this->_psType) and !$this->selectedThis ($this->_psType, $name, $id)) {

			return FALSE;

		} else {

			if(empty($this->ups_username)){

				vmError('UPS username is not set in plugin settings.');

			}

			if(empty($this->ups_server)){

				vmError('UPS server type is not set in plugin settings.');

			}

			if(empty($this->ups_path)){

				vmError('UPS path is not set in plugin settings.');

			}

			if($this->ups_reporterrors == 1) vmWarn('VMSHIPMENT_UPS_SHIP_REPORTERRORS_NOTICE');

			

			$toConvert = array('weight_start','weight_stop','orderamount_start','orderamount_stop');

			foreach($toConvert as $field){

				if(!empty($data[$field])){

					$data[$field] = str_replace(array(',',' '),array('.',''),$data[$field]);

				} else {

					unset($data[$field]);

				}

			}



			//Test to ensure conditions proper		

			if(!empty($data['zip_start']) and !empty($data['zip_stop']) and (int)$data['zip_start']>=(int)$data['zip_stop']){

				vmWarn('VMSHIPMENT_UPS_SHIP_ZIP_CONDITION_WRONG');

			}

			if(!empty($data['weight_start']) and !empty($data['weight_stop']) and (float)$data['weight_start']>=(float)$data['weight_stop']){

				vmWarn('VMSHIPMENT_UPS_SHIP_WEIGHT_CONDITION_WRONG');

			}



			if(!empty($data['orderamount_start']) and !empty($data['orderamount_stop']) and (float)$data['orderamount_start']>=(float)$data['orderamount_stop']){

				vmWarn('VMSHIPMENT_UPS_SHIP_AMOUNT_CONDITION_WRONG');

			}

			

			if ($handlingType == 'PCTCART' || $handlingType == 'PCTSHIP' ){

				if (!preg_match('/%$/',$data['UPS_HANDLINGFEE'])){

					vmError('A percentage type handling fee was selected but a percentage value was not entered. Example: "5%".');

				}

			}



			return $this->setOnTablePluginParams ($name, $id, $table);

		}

	}

	/**

	*  Validate data for plugin shipment in BE older VM versions

	*/

	function plgVmSetOnTablePluginParamsShipment ($name, $id, &$table) {

		//vmdebug('plgVmSetOnTablePluginShipment');

		if (!empty($this->_psType) and !$this->selectedThis ($this->_psType, $name, $id)) {

			return FALSE;

		} else {	

			return $this->setOnTablePluginParams ($name, $id, $table);

		}

	}

	



	/**

	* This method executed to API call to UPS.

	* @param                $order_weight - The actual weight in the cart (prior to any padding)

	* @param                $cart - An array of the cart

	* @param                $destaddress - An array of the destination address that you are shipping to

	* @return currency

	*/

	private function _getUPSRates($order_weight, $cart, $destaddress, $cartvalue, $ups_service_code) {

		$dest_zip = $destaddress['zip'];

		$dest_countryid = $destaddress['virtuemart_country_id'];

		$dest_stateid = $destaddress['virtuemart_state_id'];

		//Setup vendor address data

		$vendoraddress = $this->_getVendorAddress($cart->vendorId);

		$source_zip = $vendoraddress->zip;

		$source_countryid = $vendoraddress->virtuemart_country_id;

		$source_stateid = $vendoraddress->virtuemart_state_id;

				

		//vmdebug('**UPS API CALL: Service='.$ups_service_code.' Cart weight='.$order_weight.'lbs zipcode from='.$source_zip.' zipcode to='.$dest_zip.' countryid='.$dest_countryid);

		$this->logInfo('**UPS API CALL: Service='.$ups_service_code.' Cart weight='.$order_weight.'lbs zipcode from='.$source_zip.' zipcode to='.$dest_zip.' countryid='.$dest_countryid);

		

		//Store variables to cart which are used to determine if another call to UPS API is required on future requests.

		$_SESSION['cart']['ups_ship_weight'] = $order_weight;

		$_SESSION['cart']['ups_ship_source_zip'] = $source_zip;

		$_SESSION['cart']['ups_ship_dest_zip'] = $dest_zip;

		

		//$this->ups_errored = true; //default to false

		$this->ups_api_status[$ups_service_code] = 'error'; //default to error

		

		if($this->ups_reporterrors == 1) $ups_reporterrors = 1;

		else $ups_reporterrors = 0;

		

		if($order_weight > 0) {

			$ups_packagesize = $this->ups_packagesize;

			$ups_packageid = 0;				

			//Pad the shipping weight to allow weight for shipping materials

			$ups_padding = $this->ups_padding;

			$ups_padding = $ups_padding * 0.01;

			$order_weight = ($order_weight * $ups_padding) + $order_weight;

			$order_weight = number_format($order_weight, 3); //go to 3 decimals

			if ($order_weight < 0.1) $order_weight = 0.1; //UPS requires at least 0.1 pounds

			//Place order weight in proper unit of measurement base on plugin settings (If 'SUREPOST < 1LBS (92)' then always OZ)

			if($ups_service_code == '92'){

				$weight_measure = 'OZ';

			}else{

				$weight_measure = $this->ups_weightunit;

			}

			$order_weight = ShopFunctions::convertWeigthUnit($order_weight, 'LB', $weight_measure);

			

			$this->logInfo('**Cart weight with padding & unit='.$order_weight.$weight_measure);

					

			//determine residential indicator 

			$ups_residentialaddress=true; //default to true

			switch ($this->ups_residentialindicator) {

				case "0": //auto - check ship to company field

					if(trim($destaddress['company']) !== "") $ups_residentialaddress=false;

					break;

				case "2": //never residential

					$ups_residentialaddress=false;

			}



			//Title for your request

			$request_title = "Rating and Service";

			//$request_title = "Shipping Estimate";

			

			if(empty($source_countryid)) {

				if($ups_reporterrors) vmWarn('VMSHIPMENT_UPS_SHIP_SOURCE_COUNTRY_ID_EMPTY');

				return;

			}

			if(empty($dest_countryid)) {

				if($ups_reporterrors) vmWarn('VMSHIPMENT_UPS_SHIP_COUNTRY_ID_EMPTY');

				return;

			}

			if(empty($source_zip)) {

				if($ups_reporterrors) vmWarn('VMSHIPMENT_UPS_SHIP_SOURCE_ZIP_EMPTY');

				return;

			}

			if(empty($dest_zip)) {

				if($ups_reporterrors) vmWarn('VMSHIPMENT_UPS_SHIP_DEST_ZIP_EMPTY');

				return;

			}

			

			$dest_country = ShopFunctions::getCountryByID ($dest_countryid, 'country_2_code');

			$dest_country_name = ShopFunctions::getCountryByID ($dest_countryid);

			$dest_state = ShopFunctions::getStateByID ($dest_stateid, 'state_2_code');

			$source_country = ShopFunctions::getCountryByID ($source_countryid, 'country_2_code');

			$source_country_name = ShopFunctions::getCountryByID ($source_countryid);

			$source_state = ShopFunctions::getStateByID ($source_stateid, 'state_2_code');

			$currency_code_3 = $cart_currency_code = ShopFunctions::getCurrencyByID ($cart->pricesCurrency, 'currency_code_3');

			//Delivery confirmation

			$deliveryconfirmation = (int)$this->ups_deliveryconfirmation;

			if($deliveryconfirmation > 0){

				if($dest_country !== "US")

					$deliveryconfirmation = $deliveryconfirmation-1; //outside the US confirmation requires signature so options are void 1

			}

			

			//If weight is over 150 pounds, can not send UPS so write notice (update in the future to be able to split the package)

			if( $order_weight > 150.00 ) {

				if($ups_reporterrors) vmWarn( JText::sprintf('VMSHIPMENT_UPS_SHIP_WEIGHT_GT150', $order_weight));

				return;

			}else{

				//Build XML string based on service request

				$xmlPost  = "<?xml version=\"1.0\"?>";

				$xmlPost .= "<AccessRequest xml:lang=\"en-US\">";

				$xmlPost .= " <AccessLicenseNumber>".$this->ups_accesskey."</AccessLicenseNumber>";

				$xmlPost .= " <UserId>".$this->ups_username."</UserId>";					

				$xmlPost .= " <Password>".$this->ups_password."</Password>";

				$xmlPost .= "</AccessRequest>";

				$xmlPost .= '<?xml version="1.0"?'.'>';

				$xmlPost .= "<RatingServiceSelectionRequest xml:lang=\"en-US\">";

				$xmlPost .= " <Request>";

				$xmlPost .= "  <TransactionReference>";

				$xmlPost .= "  <CustomerContext>".$request_title."</CustomerContext>";

				$xmlPost .= "  <XpciVersion>1.0001</XpciVersion>";

				$xmlPost .= "  </TransactionReference>";

				$xmlPost .= "  <RequestAction>rate</RequestAction>";

				//$xmlPost .= "  <RequestOption>shop</RequestOption>";

				if($ups_service_code == 'All'){

					$xmlPost .= "  <RequestOption>shop</RequestOption>";

				}else{

					$xmlPost .= "  <RequestOption>rate</RequestOption>";

				}

				$xmlPost .= " </Request>";

				$xmlPost .= " <PickupType>";

				//$xmlPost .= "  <Code>".$this->ups_pickuptype."</Code>";

				//2/18/2014- SurePost support

				if($ups_service_code !== 'All'){

					$xmlPost .= "  <Code>01</Code>";

				}else{

					$xmlPost .= "  <Code>".$this->ups_pickuptype."</Code>";

				}

				$xmlPost .= " </PickupType>";

				$xmlPost .= " <CustomerClassification>";

				$xmlPost .= "  <Code>".$this->ups_customerclassification."</Code>";

				$xmlPost .= " </CustomerClassification>";

				$xmlPost .= " <Shipment>";

				if($this->ups_customerclassification == '00') {

					$xmlPost .= " <RateInformation><NegotiatedRatesIndicator/></RateInformation>";

				}

				//2/18/2014- SurePost support

				if($ups_service_code !== 'All'){

					//$xmlPost .= "<Service><Code>".$ups_service_code."</Code><Description>Parcel Select</Description></Service>";

					$xmlPost .= "<Service><Code>".$ups_service_code."</Code></Service>";

				}

				$xmlPost .= "  <Shipper>";

				$xmlPost .= "   <ShipperNumber>".$this->ups_accountnumber."</ShipperNumber>";

				$xmlPost .= "   <Address>";

				$xmlPost .= "    <PostalCode>".$source_zip."</PostalCode>";

				$xmlPost .= "    <CountryCode>".$source_country."</CountryCode>";

				$xmlPost .= "    <City>".$vendoraddress->city."</City>";

				$xmlPost .= "    <StateProvinceCode>".$source_state."</StateProvinceCode>";

				$xmlPost .= "   </Address>";

				$xmlPost .= "  </Shipper>";

				$xmlPost .= "  <ShipTo>";

				$xmlPost .= "   <Address>";

				$xmlPost .= "    <AddressLine1>".$destaddress['address_1']."</AddressLine1>";

				$xmlPost .= "    <PostalCode>".$dest_zip."</PostalCode>";

				$xmlPost .= "    <CountryCode>$dest_country</CountryCode>";

				$xmlPost .= "    <City>".$destaddress['city']."</City>";

				$xmlPost .= "    <StateProvinceCode>".$dest_state."</StateProvinceCode>";

				if($ups_residentialaddress)	$xmlPost .= "    <ResidentialAddressIndicator/>";

				$xmlPost .= "   </Address>";

				$xmlPost .= "  </ShipTo>";

				$xmlPost .= "  <ShipFrom>";

				$xmlPost .= "   <Address>";

				$xmlPost .= "    <PostalCode>".$source_zip."</PostalCode>";

				$xmlPost .= "    <CountryCode>".$source_country."</CountryCode>";

				$xmlPost .= "    <City>".$vendoraddress->city."</City>";

				$xmlPost .= "    <StateProvinceCode>".$source_state."</StateProvinceCode>";

				$xmlPost .= "   </Address>";

				$xmlPost .= "  </ShipFrom>";

				$xmlPost .= "  <Package>";

				$xmlPost .= "   <PackagingType>";

				$xmlPost .= "    <Code>".$this->ups_packagingtype."</Code>";

				//$xmlPost .= "    <Code>02</Code>";

				$xmlPost .= "   </PackagingType>";

				//$xmlPost .= "   <Dimensions>";

				//$xmlPost .= "    <UnitOfMeasurement>";

				//$xmlPost .= "     <Code>IN</Code>";

				//$xmlPost .= "    </UnitOfMeasurement>";

				//$xmlPost .= "     <Length></Length>";

				//$xmlPost .= "     <Width></Width>";

				//$xmlPost .= "     <Height></Height>";

				//$xmlPost .= "   </Dimensions>";					

				$xmlPost .= "   <PackageWeight>";

				$xmlPost .= "    <UnitOfMeasurement>";

				//$xmlPost .= "     <Code>".($weight_measure == 'KG' ? 'KGS' : 'LBS')."</Code>";

				$xmlPost .= "     <Code>".($weight_measure == 'KG' ? 'KGS' : ($weight_measure == 'OZ' ? 'OZS' : 'LBS'))."</Code>";

				$xmlPost .= "    </UnitOfMeasurement>";

				$xmlPost .= "    <Weight>".$order_weight."</Weight>"; 

				$xmlPost .= "   </PackageWeight>";

				$xmlPost .= "   <PackageServiceOptions>";

				if($this->ups_declaredvalue == '1') {		

					$xmlPost .= "     <InsuredValue>";

					$xmlPost .= "       <CurrencyCode>".$currency_code_3."</CurrencyCode>";

					$xmlPost .= "       <MonetaryValue>".$cartvalue."</MonetaryValue>";

					$xmlPost .= "     </InsuredValue>";

				}

				if(($deliveryconfirmation > 0) && ($dest_country == "US")){

					$xmlPost .= "     <DeliveryConfirmation>";

					$xmlPost .= "       <DCISType>".$deliveryconfirmation."</DCISType>";

					$xmlPost .= "     </DeliveryConfirmation>";

				}

				$xmlPost .= "   </PackageServiceOptions>";

				$xmlPost .= "  </Package>";

				if(($deliveryconfirmation > 0) && ($dest_country !== "US")){

					$xmlPost .= "   <ShipmentServiceOptions>";

					$xmlPost .= "     <DeliveryConfirmation>";

					$xmlPost .= "       <DCISType>".$deliveryconfirmation."</DCISType>";

					$xmlPost .= "     </DeliveryConfirmation>";

					$xmlPost .= "   </ShipmentServiceOptions>";

				}

				$xmlPost .= " </Shipment>";

				$xmlPost .= "</RatingServiceSelectionRequest>";

				

				//$host = $this->ups_server;

				if($this->ups_server == "STAGING"){

					$host = 'wwwcie.ups.com';

				}else{

					$host = 'onlinetools.ups.com';

				}

				

				$path = $this->ups_path;

				$port = 443;

				$protocol = "https";



				// Using cURL is Up-To-Date and easier!!

				if( function_exists( "curl_init" )) {

					$CR = curl_init();

					curl_setopt($CR, CURLOPT_URL, $protocol."://".$host.$path);

					curl_setopt($CR, CURLOPT_POST, 1);

					curl_setopt($CR, CURLOPT_FAILONERROR, true);

					curl_setopt($CR, CURLOPT_POSTFIELDS, $xmlPost);

					curl_setopt($CR, CURLOPT_RETURNTRANSFER, 1);

					curl_setopt ($CR, CURLOPT_SSL_VERIFYPEER, FALSE);

					curl_setopt ($CR, CURLOPT_CONNECTTIMEOUT, 20);

					curl_setopt ($CR, CURLOPT_TIMEOUT, 30);

					if (!empty($this->ups_proxyserver)){

						curl_setopt ($CR, CURLOPT_HTTPPROXYTUNNEL, TRUE);

						curl_setopt ($CR, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);

						curl_setopt ($CR, CURLOPT_PROXY, $this->ups_proxyserver);

					}

					$xmlResult = curl_exec( $CR );

					$error = curl_error( $CR );

					curl_close( $CR );

				

				//HTTP Post

				}else{

					$protocol = "ssl";

					$fp = fsockopen("$protocol://".$host, $port, $errno, $errstr, $timeout = 60);

					if( !$fp ) {

						$error = _UPS_RESPONSE_ERROR.": $errstr ($errno)";

					}else{

						//send the server request

						fputs($fp, "POST $path HTTP/1.1\r\n");

						fputs($fp, "Host: $host\r\n");

						fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");

						fputs($fp, "Content-length: ".strlen($xmlPost)."\r\n");

						fputs($fp, "Connection: close\r\n\r\n");

						fputs($fp, $xmlPost . "\r\n\r\n");



						$xmlResult = '';

						while(!feof($fp)) {

							$xmlResult .= fgets($fp, 4096);

						}

						// Cut off the HTTP Header and get the Body

						$xmlResult = trim(substr( $xmlResult, strpos( $xmlResult, "\r\n\r\n" )+2 ));

					}

				}

				

				//Display textarea fields when in debug mode

				if(VMConfig::showDebug()  ){

					echo 'XML Post: <br /><textarea cols="120" rows="5">'.$protocol.'://'.$host.$path.'?'.$xmlPost.'</textarea><br />XML Result:<br /><textarea cols="120" rows="12">'.$xmlResult.'</textarea><br />Cart Contents: '.$order_weight.'<br /><br/>';

				}

				$this->logInfo($xmlPost);

				$this->logInfo($xmlResult);

				

				//Check for error from response from UPS

				if(!empty($error)) {

					if ($ups_reporterrors) vmWarn('UPS Error: '.$error);

					vmDebug($error);

					$this->logInfo($error);

					return;

				}

				//Parse XML response from UPS

				$xmlDoc = new SimpleXMLElement($xmlResult);

				

				//Check for error in response

				if( strstr( $xmlResult, "Failure" ) ) {

					$error = $xmlDoc->Response->Error->ErrorCode;

					if(empty($error)) $error = $xmlResult; //catch entire xml if no specific error found

					$error = 'UPS Response Error '.$error.': '.$xmlDoc->Response->Error->ErrorDescription;

					if ($ups_reporterrors) vmWarn($error);

					vmDebug($error);

					$this->logInfo($error);

					return;

				}

				

				//Get all shipping options from XML response

				//2/18/2014 - add on to existing array now that multiple calls can be made to UPS for SurePost

				//$count = 0;

				$count = count($this->ship_service);

				foreach ($xmlDoc->RatedShipment as $postage) {

					$serviceName = $postage->Service->Code;

					$this->ship_service[$count] = $serviceName;

					//Support for negotiated rates

					if(($this->ups_customerclassification == '00') && !empty($postage->NegotiatedRates->NetSummaryCharges->GrandTotal->MonetaryValue)) {

						$this->ship_postage[$count] = $postage->NegotiatedRates->NetSummaryCharges->GrandTotal->MonetaryValue;

					}else{

						$this->ship_postage[$count] = $postage->TotalCharges->MonetaryValue;

					}

					$count++;

				}

			}

		}else{

			vmWarn ('VMSHIPMENT_UPS_SHIP_WEIGHT_LT0');

			return;

		}



		//$ups_errored = false;

		$this->ups_api_status[$ups_service_code] = 'success';

		return;

	} //end function getUPSRates



}

?>

