<?php
/**
 *****************************************************************************************************************************************************
 * Google Tag Manager
 * Google Tag Manager (GTM) is plugin to insert GTM code into the site to be able to easily insert analytics and other google magic into your site.
 *
 * @package    Joomla 2.5.0
 * @author     EasyJoomla.org <support@easyjoomla.org>
 * @copyright  2014 EasyJoomla.org
 * @license    http://opensource.org/licenses/GPL-3.0 GPL-3.0
 * @link       http://www.easyjoomla.org
 * @generated  2014-07-30 at 13-42-27
 * @generator  Easy Joomla Extensions Generator by Viktor Jelínek <vikijel@gmail.com> at EasyJoomla.org <support@easyjoomla.org>
 *
 *****************************************************************************************************************************************************
 */
defined('_JEXEC') or die('Restricted access');

if (!class_exists('GtmQueue'))
{
	require JPATH_PLUGINS . '/system/gtm/helpers/queue.php';
}

/**
 * Class plgSystemGtm
 */
class plgSystemGtm extends JPlugin
{
	/**
	 * @return bool|null
	 */
	public function onAfterRender()
	{
		$container_id = trim($this->params->get('container_id', ''));

		if ((!(int) $this->params->get('enable_backend', 1) and JFactory::getApplication()->isAdmin()) or $container_id == '')
		{
			return null;
		}

		if (!(int) $this->params->get('enable_any_format', 0) and JFactory::getApplication()->input->get('format') != '')
		{
			return null;
		}

		if (!(int) $this->params->get('enable_tmpl_component', 0) and JFactory::getApplication()->input->get('tmpl') == 'component')
		{
			return null;
		}

		$queue       = new GtmQueue();
		$items       = $queue->getItems();
		$tracked_ids = array();
		$tag         = array("<!-- Begin: gtm -->");
		$tag[]       = '<script type="text/javascript">';
		$tag[]       = 'dataLayer = [];';

		if (!empty($items))
		{
			foreach ($items as $item)
			{
				if (isset($item->json) and $item->json != '')
				{
					$tag[]         = 'dataLayer.push(' . $item->json . ');';
					$tracked_ids[] = $item->id;
				}
			}
		}

		$tag[] = "</script>";
		$tag[] = "<noscript><iframe src=\"//www.googletagmanager.com/ns.html?id=$container_id\" height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>";
		$tag[] = "<script type=\"text/javascript\">";
		$tag[] = "(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({";
		$tag[] = "	'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],";
		$tag[] = "	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';";
		$tag[] = "	j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);";
		$tag[] = "})(window,document,'script','dataLayer','$container_id');";
		$tag[] = "</script>";
		$tag[] = "<!-- End: gtm -->";

		$tag   = implode("\n", $tag);
		$body  = JResponse::getBody();
		$start = stripos($body, '<body');
		$end   = stripos($body, '>', $start);
		$body  = substr_replace($body, $tag, $end + 1, 0);

		JResponse::setBody($body);

		$queue->updateItemsStatus($tracked_ids);

		if ((int) $this->params->get('write_log', 0))
		{
			jimport('joomla.log.log');
			JLog::addLogger(array('text_file' => 'plg_system_gtm.log.php'), JLog::ALL, array('plg_system_gtm'));
			JLog::add(__METHOD__ . ": \n" . $tag, JLog::INFO, 'plg_system_gtm');
		}

		return true;
	}
}
